---
title: Building the Frame
description: Build instructions for assembling the X301 Frame
published: true
date: 2022-10-04T00:02:18.308Z
tags: guides, build, frame, guide, moved
editor: markdown
---

This page has been moved [here](/printers/x301/build/frame).