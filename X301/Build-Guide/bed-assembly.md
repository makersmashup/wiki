---
title: Assemble the Bed
description: Step-by-Step instructions for Assembling the Bed
published: true
date: 2022-10-03T23:27:18.359Z
tags: guide, bed, heatbed, assemble
editor: markdown
---

This page has been moved [here](/printers/x301/build/bed).