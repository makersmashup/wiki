---
title: Build Guide
description: Instructions for building your LayerFused X301 Printer
published: true
date: 2022-10-03T22:51:21.895Z
tags: 
editor: markdown
---

This page has been moved [here](https://wiki.layerfused.com/e/en/printers/x301).