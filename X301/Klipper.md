---
title: Klipper on X301
description: Klipper Firmware and extras on LayerFused X301 
published: true
date: 2022-10-03T23:36:13.651Z
tags: 
editor: markdown
---

This page has been moved [here](/general/firmware/klipper).