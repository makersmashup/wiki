---
title: ADXL345
description: The ADXL345 is used in Klipper to tune Resonance and used with Input Shaper
published: true
date: 2022-10-07T00:41:04.218Z
tags: 
editor: markdown
---

This page has been moved [here](/printers/mods/unknown/adxl345).