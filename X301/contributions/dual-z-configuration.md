---
title: Dual Z Configuration
description: Setting up Dual Z stepper Drivers and Marlin
published: true
date: 2022-10-03T23:15:05.733Z
tags: 
editor: markdown
---

This page has been moved [here](/printers/x301/mods/unknown/dual-z-axis).