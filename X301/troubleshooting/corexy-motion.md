---
title: CoreXY Motion Troubleshooting
description: Step by Step instructions on solving common CoreXY motion setup issues on the LayerFused X301
published: true
date: 2022-10-03T23:55:58.028Z
tags: x301 layerfused corexy steppers axis
editor: markdown
---

This page has been moved [here](/general/troubleshoot/corexy-motion).