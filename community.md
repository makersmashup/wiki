---
title: Community
description: 
published: true
date: 2022-10-24T15:41:37.040Z
tags: 
editor: markdown
---

# Community

If you like the LayerFused Projects, you are highly encouraged to join us on [Discord](https://discord.gg/9RqYqRMfhM)!

## Contributions

This Wiki is maintained by volunteers from the community. If you spot and issue or room for improvement, please let us know.

To maintain a sense of uniformity, please use the following conventions when adding to the Wiki:
- All URLs shoud be lower-case alphanumeric with dash (-) characters as needed
- All content should be located within the appropriate section
- Use relative links (`build/frame`, `./frame`) when working inside a small section, and absolute links when you are linking to pages outside that section (`/printers/x301/build/frame`)
- All Amazon links should conform to the standard described [here](https://affiliate-program.amazon.com/help/node/topic/GP38PJ6EUR6PFBEC) with the GET parameters `?ref=nosim&tag=rwmech-20`. For example, [https://www.amazon.com/dp/B0839461B5/?ref=nosim&tag=rwmech-20](https://www.amazon.com/dp/B0839461B5/?ref=nosim&tag=rwmech-20). Additionally, [Amazon requires that any pages with Amazon Associates links have a disclosure](https://affiliate-program.amazon.com/help/node/topic/GHQNZAU6669EZS98). If you include an Amazon Associates link, please put an `*` after the link and add the following message at the bottom of the page:
  - `\* LayerFused is an Amazon Associates and will earn a commission at no additional cost to you if you use this link`.

Section Breakdown:
- /printers/[model]/[topic]/[subtopic] (For example: /printers/x301/build/frame)