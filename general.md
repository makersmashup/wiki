---
title: General Information
description: General 3D Printer Topics
published: true
date: 2022-10-04T15:04:21.169Z
tags: 
editor: markdown
---

Information which pertains to more than one unique printer or other hardware can be found here.

## [Control Boards](control-boards)

One of the most important considerations when building a 3D Printer is the control board, which acts as the interface all the components of the 3D Printer are controlled by. There are many good options, but here are some options that would serve you well.

When selecting a board for one of the X-Series printers, keep in mind that you will want to have stepper drivers for 1 x motion, 1 y motion, 1 extruder, and 2 z motion. This will consume all 5 driver slots in most boards, so if you want to do mods like adding another z stepper for better auto-leveling or a second extruder, you will want more slots. 8 slots is a great choice for many.

Keep in mind that this is one area of 3D printing that experiences a very large amount of changes very quickly and what may be a good recommendation today might not be as good tomorrow. Make sure to do your own research before purchasing a board.

- [BIGTREETECH Octopus Pro](control-boards/btt-octopus-pro)
- [BIGTREETECH Manta M8P](control-boards/btt-manta-m8p)

## [Firmware](firmware)

3D Printers typically use a controller board running an open source firmware. Notes about common firmwares we have expored on our printers can be found here.

- [Marlin](firmware/marlin)
- [Klipper](firmware/klipper)

## [Slicers](slicers)

Once you have created your 3D model and are ready to print, you need to use a Slicer to convert the model into instructions the printer can understand. You can find notes about how to tune various slicers for the best outcome below.

[Cura](slicers/cura)

## [Stepper Drivers](stepper-drivers)

Stepper Drivers is a subject in 3D Printing that saw many changes for a period of time, but that has remained pretty static in recent years. There are a few recommended stepper drivers to consider.

- TMC2209: These are a bit more expensive (approx $6/ea), but are currently the top of the line in terms of features. This is probably the one you want.
- TMC2208: These are slightly cheaper (approx $5/ea) compared to the TMC2209, but are missing a couple of features that the TMC2209 brings. The biggest feature that is missing when compared to the TMC2208 is sensorless homing, which allows the TMC2209 to sense changes in resistance to determine the edge of a print volume. If you already have TMC2208's, it probably isn't worth the upgrade, but if you are buying new, you probably want to opt for the TMC2209. One small advantage the TMC2208 has is the support for 35V over the 28V of the TMC2209.
- TMC5160: These boards are commonly used with High Voltage (HV) stepper motors as they can handle up to 50V.
- TMC5161: These boards are currently in development. You might want to take a look at them if they are out when you are reading this.

You can find a nice detailed comparion [here](https://learn.watterott.com/silentstepstick/comparison/)

BIGTREETECH has recently made one of the larger advancements in stepper drivers seen in recent years with their EZ drivers. These drivers change the standard form with pins into something that looks more like a stick of RAM. These provide several nice features such as a built in heatsinks on both sides of the chip, no more small bendable pins, and are harder to orrient incorrectly. BTT provides EZ variations of all popular stepper drivers. With all of those benefits comes the drawback that they are only currently made by BTT and are only available on select boards or via a breakout chip. Unless you are constantly switching drivers for some reason, you might want to pass on these for the time being.

## [Tools](tools)

There is a small collection of tools which any maker would be well served to have in their arsenal. The tools listed here are required or strongly recommended for building LayerFused devices.

## [Troubleshoot](troubleshoot)

- [Stepper Motors](troubleshoot/stepper-motors)
- [Printing](troubleshoot/printing)
- [Calibration](troubleshoot/calibration)
- [CoreXY Motion](troubleshoot/corexy-motion)
- ["Err: EEPROM Version"](firmware/marlin/eeprom-error)
