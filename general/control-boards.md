---
title: Control Boards
description: 
published: true
date: 2022-10-04T14:36:28.677Z
tags: 
editor: markdown
---

One of the most important considerations when building a 3D Printer is the control board, which acts as the interface all the components of the 3D Printer are controlled by. There are many good options, but here are some options that would serve you well.

When selecting a board for one of the X-Series printers, keep in mind that you will want to have stepper drivers for 1 x motion, 1 y motion, 1 extruder, and 2 z motion. This will consume all 5 driver slots in most boards, so if you want to do mods like adding another z stepper for better auto-leveling or a second extruder, you will want more slots. 8 slots is a great choice for many.

Keep in mind that this is one area of 3D printing that experiences a very large amount of changes very quickly and what may be a good recommendation today might not be as good tomorrow. Make sure to do your own research before purchasing a board.

- [BIGTREETECH Octopus Pro](btt-octopus-pro)
- [BIGTREETECH Manta M8P](btt-manta-m8p)