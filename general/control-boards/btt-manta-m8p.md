---
title: BTT - Manta M8P
description: 
published: true
date: 2022-10-04T18:16:34.936Z
tags: control-board
editor: markdown
---

# Overview

The BIGTREETECH Manta M8P is the 8 stepper driver version of their boards which introduce a small linux based computer directly onto the control board. This is an amazing combination for those looking to run software such as OctoPrint for their 3D Printer.

It was released around July of 2022.

# Key Features

- Up to 8 stepper drivers
- Ability to use High Current (HC) stepper motors/drivers
- Supports up to 60V stepper motors
- Options for Vin/12V/5V fans (6 controllable, 2 always on)
- Socket for a Raspberry Pi CM4 or BTT CB1

# Considerations

- It is very common to want to pair a 3D Printer with a Linux based computer such as the Raspberry Pi for projects such as OctoPrint. This board has a slot which can accomodate a computer in the same form factor as the Raspberry Pi CM4. Given the difficulty of finding a CM4, the BTT CB1 has been created as a drop in replacement which can be found at a MUCH cheaper price.
- The BTT CB1 has appoximately the same specs as the Raspberry Pi 3B. The biggest missing feature is a CAN port, so you will need an adapter if you run a CAN on the toolhead.

# Additional Information

- [Official Product Page](https://biqu.equipment/products/manta-m4p-m8p)

# Pricing & Availability

At the time of writing (04OCT2022), the M8P is not quite as readily available as other boards, but it can still be found with some lead time directly from [BIQU](https://biqu.equipment/products/manta-m4p-m8p). BIQU has multiple configurations available, but the [CB1+Manta M8P+8 PCS TMC2209](https://biqu.equipment/products/manta-m4p-m8p?variant=39847239155810) combo for $100 is a pretty solid deal if you don't already have a Raspberry Pi CM4 (CB1) or TMC2209 stepper drivers. If you do, the board itself can be purchased for $50.