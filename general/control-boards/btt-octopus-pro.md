---
title: BTT Octopus Pro - Control Boards
description: 
published: true
date: 2022-10-04T14:37:18.751Z
tags: 
editor: markdown
---

# Overview

The BIGTREETECH Octopus Pro is a newer revision of their popular Octopus board and supports 8 High Current Stepper Drivers.

It was released around October 2021.

# Key Features

- Up to 8 stepper drivers
- Ability to use High Current (HC) stepper motors/drivers
- Supports up to 60V stepper motors
- Options for Vin/12V/5V fans (6 controllable, 2 always on)

# Considerations

- It is very common to want to pair a 3D Printer with a Linux based computer such as the Raspberry Pi for projects such as OctoPrint. Unfortunately those are very difficult to find for a reasonable price these days. If you do not have a Raspberry Pi for your project already, the [BTT Manta M8P](./btt-manta-m8p) is probably a better choice.
- There are two chipsets available with this board; F446 and F429. The F446 is slightly faster at 180MHz, but comes with 512KB of flash compared to the 168MHz F429 with 1MB of flash. The 2x increase in RAM is likely more beneficial than the 12MHz, but that is a decision you will need to make. As a note, some 3D printer firmwares are already over 512KB with all features enabled and they will only continue to grow in size.

# Additional Information

- [Official Product Page](https://biqu.equipment/products/bigtreetech-octopus-pro-v1-0-chip-f446?variant=39482177257570)
- [Detailed Review by Make'n'Print](https://www.makenprint.uk/3d-printing/3d-printing-guides/3d-printer-mainboard-installation-guides/btt-octopus-guides/btt-octopus-pro-guide)

# Pricing & Availability

The Octopus Pro is readily available for purchase directly from [BIQU](https://biqu.equipment/products/bigtreetech-octopus-pro-v1-0-chip-f446), [Amazon](https://www.amazon.com/BIGTREETECH-Controller-Motherboard-Compatible-STM32F429ZGT6/dp/B09HX6RM3Q), and many other online retailers for approximately $70 as of 04OCT2022.