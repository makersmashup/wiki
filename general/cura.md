---
title: Cura
description: Cura profiles for LayerFused Printers
published: true
date: 2022-10-04T00:10:59.684Z
tags: slicer, cura, profile
editor: markdown
---

This page has been moved [here](/general/slicer/cura).