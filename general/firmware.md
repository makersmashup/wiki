---
title: Firmware - General
description: 
published: true
date: 2022-10-04T17:45:06.285Z
tags: marlin, firmware, general, klipper
editor: markdown
---

3D Printers typically use a controller board running an open source firmware. Notes about common firmwares we have expored on our printers can be found here.

[Marlin](marlin)
[Klipper](klipper)