---
title: Tuning - Klipper
description: 
published: true
date: 2022-10-07T00:43:55.188Z
tags: 
editor: markdown
---

# Tuning Klipper
Tuning Klipper settings on your X-Series printer.

## Probe Calibration and Bed Mesh

## Z offset and E-steps

## Pressure Advance

## Resonance Compensation tuning
[ADXL345](adxl345)

## Acceleration