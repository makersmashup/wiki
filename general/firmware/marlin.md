---
title: Marlin - General
description: 
published: true
date: 2022-10-04T00:23:54.992Z
tags: marlin, firmware, general
editor: markdown
---

# LayerFused Sources
The LayerFused team maintains a fork of Marlin for each printer.

[C201](https://bitbucket.org/makersmashup/layerfused-c201)
[X301](https://bitbucket.org/makersmashup/marlin)

# G-Code

[G-Code Commands & Scripts](g-code)

# Guides

[Sensorless Homing](sensorless-homing)

# Troubleshooting

[EEPROM Version Error](eeprom-error)