---
title: EEProm Version Error
description: 
published: true
date: 2022-10-04T00:07:29.672Z
tags: 
editor: markdown
---

# EEPROM Version Error
This error is caused when the firmware version is different than the EEPROM version in Marlin.  This is completely normal for new firmware installs onto your mainboard. 

The exact message displayed is `Err: EEPROM Version` as ssen in the image below.

![eeprom_error.png](/eeprom_error.png)

To resolve this error on your firmware execute the following commands via Pronterface or another terminal interface.
```
M502 ; Load Factory Defaults from Firmware & Reset
M500 ; Save Settings to EEPROM
```

This will resolve the error and wont display the message on next startup. 