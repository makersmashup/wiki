---
title: G-Code Reference & Scripts
description: 
published: true
date: 2022-10-03T23:39:45.895Z
tags: 
editor: markdown
---

# G-Code
This page contains common gcode commands and example starting and ending scripts for LayerFused Printers. Also note that these commands are for Marlin Firmware.  To send G-code commands to your printer you will need a terminal interface.  There are many options to do this and even some Slicer applications do this.  Pronterface and Octoprint are good ones to use.

---
## Common G-code commands
#### Movement G-codes
**G0 - G1 - Linear Move**
Description: Add a linear move to the queue after all previous moves are completed. This type of move traces a straight line from point A to point B ensuring that all specified axes arrive at the same time.
Usage: `G0 [E<pos>] [F<rate>] [X<pos>] [Y<pos>] [Z<pos>]`
Example: `G0 F3000 Z25 ; Move Z to 25mm with feedrate of 3000mm/m`

**G28 - Auto Home**
Description: Home one or more axes.
Usage: `G28 [X] [Y] [Z]`
Example: `G28 XY ; Home X and Y axes`
**G90 - Absolute Positioning**
Description: All coordinates given in G-code are interpreted as positions in the logical coordinate space. This is the default for Marlin.
Usage: `G90`
Example: `G90 ; Set all axes to absolute`
**G91 - Relative Positioning**
Description: All coordinates are interpreted as relative to last position.
Usage: `G91`
Example: `G91 ; Set all axes to relative`

---
#### Settings
**M92 - Set Axis Steps-per-unit**
Description: This command sets steps-per-unit for one or more axes.
Usage: `M92 [E<steps>] [X<steps] [Y<steps>] [Z<steps>]`
Example: `M92 Z800 ; sets the steps-per-unit on the Z axes to 800`
**M500 - Save Settings**
Description: Save all configurable settings to EEPROM
Usage: `M500`
Example: `M500`
**M501 - Restore Settings**
Description: Loads all saved settings from EEPROM
Usage: `M501`
Example: `M501`
**M502 - Factory Reset**
Description: Restores the configurable settings to factory defaults.
Usage: `M502`
Example: `M502 ; reset!`
**M503 - Report Settings**
Description: Displays a report of the current EEPROM settings.
Usage: `M503`
Example: `M503`
**M504 - Validate EEPROM Contents**
Description: Validates contents of the EEPROM. If error "Initialize EEPROM Failed" use this.
Usage: `M504`
Example: `M504`
**M914 - TMC Bump Sensitivity**
Description: Some TMC stepper drivers can detect when they bump into something.  This is used for Sensorless Homing. The TMC 2209 has this feature and emulates traditional EndStops.
> Compatible with TMC2130 and TMC2209.
TMC2209: Sensitivity 0-255
Others: Sensitivity +63 to -64
{.is-info}

Usage: `M914 [X<int>] [Y<int>] [Z<int>]`
Example: `M914 X45 Y36 ; set sensitivity for X axi to 45 and Y axis to 36`

---
#### Misc
**M104 - Set Hotend Tempature**
Description: Set the hotend tempature to desired temp.
Usage: `M104 [S<temp>]`
Example: `M104 S210 ; Set active hotend to 210 degrees`

**M114 - Get Current Position**
Description: Display current position of the active nozzle.
Usage: `M114`
Example: `M114`

**M119 - Endstop States**
Description: Display the current status of the endstops. "TRIGGERED" or "OPEN"
Usage: `M119`
Example: `> M119` \
`Reporting endstop status`
`x_min: open`
`y_min: open`
`z_min: TRIGGERED`

**M140 - Set Bed Tempature**
Description: Set the bed tempature to desired temp.
Usage: `M140 [S<temp>]`
Example: `M140 S70 ; Set the bed tempature to 70 degrees`

## Example Starting and Ending Scripts
There are many different variations of starting and ending scripts.  What these scripts do depends on what you want to happen before you start a print and after your print is done.  

---
### Custom Start G-Code
`G28 ; Home all axis`
`G1 Z10 F3000 ; Move Z down 10mm`
`; Prime the Extruder`
`G92 E0 ; Reset Extruder to 0`
`G1 F200 E3 ; Extrude 3mm of plastic`
`G92 E0 ; Reset Extruder to 0 again`
`M117 Printing... ; Display printing message on LCD`

This script will home the printer then move the Z axis 10mm and prime the extruder before starting the print.  There are a number of different processes that you can perform before starting such as implementing a nozzle wipe to ensure the flow of plastic is sufficent.  If your machine is equipped with a leveling sensor you could issue a `G29` to probe the bed and make sure that it is level.  With `M117` you can display whatever message you would like on the LCD.

### Custom End G-Code
One of the most important things that you should do with the end G-Code is ensure that you turn off the power to the Hotend and Bed heaters.  Not only is this for safety but also good practice so you do not burn up any parts.  One of the other benefits is you are able to either lower the build plate or push it forward for easy part removal.

`M104 S0 ; Turn off the Hotend`
`M140 S0 ; Turn off the Heat Bed`
`; Retract the Filament`
`G92 E1 ; Set extruder position to 1`
`G1 E-1 F3000 ; Retract filament by 2`
`M211 S1 ; Make sure Soft Endstops are turned on`
`G91 ; Set relative position for all axes`
`G0 Z10 F3000 ; Move the Z axis 10mm from current location`
`G90 ; Set Absolute position for all axis`
`G0 X300 Y300 F3000 ; Move the X and Y axis out of the way`
`G0 Z250 ; Move the Z axis to 250mm for easy removal`
`M150 U255 ; Light up Green after complete`
