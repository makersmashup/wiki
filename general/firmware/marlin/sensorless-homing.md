---
title: Sensorless Homing
description: 
published: true
date: 2022-10-04T00:04:00.976Z
tags: 
editor: markdown
---

# Marlin Sensorless Homing
Some TMC stepper drivers can detect when they bump into something that causes them to stop moving. This functionality is used in marlin to  take the place of traditional endstops. It is recommended that you only use this for your X and Y and then use something like a BLTOUCH for your Z Probe precision. This page describes some of the key concepts in setting up sensorless homing. 

## Hardware Configuration
Sensorless homing requires drivers like the TMC2209 and a compatible mainboard which can relay the sensing of the stall back to the marlin firmware. On the SKR this is done through the two additional DIAG pins the TMC2209 step sticks and the 2 addtional pins on the SKR board. Consult your mainboard documentation and step stick documentation for details in setting this up on other hardware configurations. 

## Marlin Configuration
Once you have a valid hardware configuration in order to make sensorless homing work correctly you will need to enable it in your firmware and recompile.  

` #define SENSORLESS_HOMING // StallGuard capable drivers only`

This enables the sensorless homing from within marlin.  The code below is then enabled in Marlin allowing you to configure the sensorless homing. 


```C
  #if EITHER(SENSORLESS_HOMING, SENSORLESS_PROBING)
    // TMC2209: 0...255. TMC2130: -64...63
    #define X_STALL_SENSITIVITY  52
    #define X2_STALL_SENSITIVITY X_STALL_SENSITIVITY
    #define Y_STALL_SENSITIVITY  52
    #define Y2_STALL_SENSITIVITY Y_STALL_SENSITIVITY
    //#define Z_STALL_SENSITIVITY  8
    //#define Z2_STALL_SENSITIVITY Z_STALL_SENSITIVITY
    //#define Z3_STALL_SENSITIVITY Z_STALL_SENSITIVITY
    //#define Z4_STALL_SENSITIVITY Z_STALL_SENSITIVITY
    //#define SPI_ENDSTOPS              // TMC2130 only
    #define IMPROVE_HOMING_RELIABILITY
  #endif
```
 
###  Sensitivity Configuration
The Stall or Bump sensitivity for sensorless homing must be configured for proper operation.  There are a few variables that affect how Marlin will home your printer. These variables are

- Homing Speed
- Build design
- Driver Current
- Bump Sensitivity (In firmware or through M914)

Optimally you want your printer to move to the zero position of the X or Y axis without slamming into the end or causing belt jumps.  The variables above will adjust that and it will take some time to adjust these to optimal values. 

#### Homing Speed
If you're using the defualt X301, X201 or X501 firmware these values are set at values are set at slower homing speeds already. Otherwise in your marlin configuration.h you can change these values. 

`#define HOMING_FEEDRATE_XY (15*60)`
`#define HOMING_FEEDRATE_Z  (4*60)`

Changing these values will adjust the speed of your homing in mm/min.  Lower values will take longer to home but reduce any slamming into the end of the carriage. 

#### Build design
Build deisgn plays an important role in sensorless homing.  CoreXY systems will see different sensitivty values based on belt tightness and position on the print bed.  In a typical cartesian system the sensitivty may be different because of bed weight vs carriage weight. So the design of the build will play a part in how the steppers identify a stall. 

#### Driver Current
The TMC Drivers allow you to adjust the current on the fly with the `M906` command.  You can then save any adjustments with `M500`. The higher the driver current the higher the sensitivity is necessary.  Since more current is flowing through the stepper is applies more force to moving the belt and carriage.  This means that it takes more effort to "Stall" the stepper.  You will need more sensitivity to prevent slamming or belt jumps at higher current ratings. You should use the least amount of current necessary to prevent belt skips. Using too much current will cause your steppers to generate more heat.  Always refer to your steppers max current rating and start with a value approximately 60% of its max current.  So a 1A stepper rating should only use around 600ma of current to start.  Your step sticks also have a maximum current rating and exceeding it will damage your stepper driver.  So using the least amount of current necessary is a best practice. 

#### Bump Sensitivity
The final part of the sensorless homing equation is the bump or stall sensitivity.  These values need to be adjusted along with the other two to find the sweet spot for your build. 

From the code block above we can adjust our stall sensitivity for X and Y in the firmware or with `M914` in runtime.

`#define X_STALL_SENSITIVITY  52`
`#define Y_STALL_SENSITIVITY  52`

These values are set in the LayerFused firmware at 52 by default.  These values work well as starting points for the X501, X301 or X201.  It is not unusual for these values to be set at vastly different values per axis.  For example a setting of `50` for the X axis and a setting of `30` for the Y axis is completely normal.  You will want to also test these values from multiple locations in your design.  For example you will want to test from the center of the bed, from the X and Y max values, X max, Y zero, etc.  Testing from various locations may result in further tunining of these values to obtain the proper values for the entire build surface. 

Consult the documentation on [M914 - TMC Bump Sensitivity](https://marlinfw.org/docs/gcode/M914.html) in the Marlin Documentation for more information on adjusting it in runtime. 