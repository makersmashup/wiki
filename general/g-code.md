---
title: G-Code
description: Common G-code commands and Scripts
published: true
date: 2022-10-03T23:40:12.221Z
tags: terminal, gcode, script
editor: markdown
---

This page has been moved [here](/general/firmware/marlin/g-code).