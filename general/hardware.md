---
title: Hardware
description: General Hardware Topics
published: true
date: 2022-10-03T19:07:11.022Z
tags: 
editor: markdown
---

# LayerFused Hardware

> This page does not contain everything available from the Layerfused shop. Things are still being added.
{.is-warning}


The following guides provide assembly instructions as well as troubleshooting guidance for all LayerFused electronics kits.

[Lighted 3D Printer Bed Kit](/general/hardware/neopixel-lighted-bed)