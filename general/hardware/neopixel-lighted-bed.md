---
title: Lighted 3D Printer Bed Kit
description: Instructions for assembling the Lighted Bed Kit
published: true
date: 2022-10-03T19:33:38.161Z
tags: neopixels, lighted bed kit, led, leds
editor: markdown
---

# Lighted Bed Kit

This page has been moved [here](/hardware/lighted-bed-kit).