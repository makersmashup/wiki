---
title: V6 Hot End
description: Information on E3D V6 Style Hot Ends
published: true
date: 2020-07-17T18:58:18.126Z
tags: hardware
editor: markdown
---

# V6 Style Hot Ends

## Troubleshooting

### Filament Jamming
Filament Jams can be caused by a number of issues and they can occur in various places. Typically you'll see these happen in the cool end, the heat break and the nozzle. 

#### Cool End
Generally speaking you should not have jams in the cool end if your nozzle is setup correctly.  Many times with inexpensive clones many people leave out the bowden tube required for the filament path through the cool end from the extruder.   

This video from BMG shows how the geared extruder should be setup with a small bowden tube guiding the filament through the cool side of the hot end.  This will prevent jams in this part of the nozzle. 

[![BMG Extruder Hot End Installation](http://img.youtube.com/vi/dTUppEUFXVE/0.jpg)](http://www.youtube.com/watch?v=dTUppEUFXVE "Hot End Installation")