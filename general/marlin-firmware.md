---
title: Marlin Firmware
description: General Information for Marlin Firmware and LayerFused Printers
published: true
date: 2022-10-03T23:33:36.458Z
tags: 
editor: markdown
---

This page has been moved [here](/general/firmware/marlin).