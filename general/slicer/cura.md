---
title: Cura
description: 
published: true
date: 2022-10-04T00:10:41.035Z
tags: 
editor: markdown
---

# Cura Profiles

The following links will download a known working Cura Profile for the LayerFused printers.

- [LayerFused_C201_PLA_.2mm](/layerfusedc201_pla_.2mm.curaprofile)
- [LayerFused_X301_PLA_.2mm](/layerfusedx301_pla_.2mm.curaprofile)