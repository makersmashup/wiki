---
title: Slicers - General
description: 
published: true
date: 2022-10-04T00:29:52.900Z
tags: slicer, cura, general
editor: markdown
---

Once you have created your 3D model and are ready to print, you need to use a Slicer to convert the model into instructions the printer can understand. You can find notes about how to tune various slicers for the best outcome below.

[Cura](cura)