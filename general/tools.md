---
title: Tools
description: 
published: true
date: 2022-10-07T16:50:18.494Z
tags: 
editor: markdown
---

# Tools / Materials
A number of tools are required during the build.  To make it easier to ensure you have all of the tools you need, we've made a comprehensive list below.  Each step will also contain a list of tools you'll need for that specific step.

**Note**: Linked products are for reference and convenience purposes. If any links are not working, check the [X301 Bill Of Materials](https://docs.google.com/spreadsheets/d/1BkFmqMdLik8-B1qxVCviEUmhcazlHoreDtXW5ndriQE/edit?usp=sharing) for a working link.

## Required

- [Metric wreches/spanners, specifically 7 mm, 8mm and 12mm](https://www.amazon.com/dp/B08CVC5BDL/?ref=nosim&tag=rwmech-20) *
- [Wire strippers](https://www.amazon.com/dp/B081821YSV/?ref=nosim&tag=rwmech-20) *
- [Insulated terminal crimper](https://www.amazon.com/dp/B07FCX1M6Q/?ref=nosim&tag=rwmech-20) *
- [40W+ Soldering iron](https://www.amazon.com/dp/B07FCX1M6Q/?ref=nosim&tag=rwmech-20) * and [set of tips](https://www.amazon.com/dp/B077V1VND5/?ref=nosim&tag=rwmech-20)*
- [Heat shrink](https://www.amazon.com/dp/B0771K1Z7Q/?ref=nosim&tag=rwmech-20)* or [vinyl electrical tape](https://www.amazon.com/dp/B00004WCCL/?ref=nosim&tag=rwmech-20)*
- [JST-XH pin crimper](https://www.amazon.com/dp/B078WPT5M1/?ref=nosim&tag=rwmech-20)* ![lflogo.svg](/lflogo.svg =10x)
- Power drill with 5mm bit ![lflogo.svg](/lflogo.svg =10x)

### Screwdrivers

If you are looking for exactly the screwdrivers and bit to get you started, the first few links should do nicely.

- [Metric hex bits or keys, specifically 1.5, 2, 2.5 and 4mm](https://www.amazon.com/dp/B002C5FH0E/?ref=nosim&tag=rwmech-20) *
- [Precision screwdrivers/bits](https://www.amazon.com/dp/B07QYL9KJT/?ref=nosim&tag=rwmech-20) *

If you are looking for a nice high quality screwdriver set with many bits, the iFixit Toolkits are hard to beat.

- [iFixIt Moray Toolkit](https://www.amazon.com/dp/B08NWKMT8V/?ref=nosim&tag=rwmech-20) *
- [iFixIt Manta Toolkit](https://www.amazon.com/dp/B07BMM74FD/?ref=nosim&tag=rwmech-20) *

## Recommended
- 3D Printer with at least 120mm^3^ build volume
- [Craft knife](https://www.amazon.com/dp/B07VN5BN48/?ref=nosim&tag=rwmech-20) *
- [Multimeter](https://www.amazon.com/dp/B07FBF3N3Q/?ref=nosim&tag=rwmech-20) * (or [alternative](https://www.amazon.com/dp/B07H9WVJDS/?ref=nosim&tag=rwmech-200) *)
- [Heat gun](https://www.amazon.com/gp/product/B00EU2T8GG/?ref=nosim&tag=rwmech-20) *
- [Ferrules with crimper](https://www.amazon.com/dp/B0839461B5/?ref=nosim&tag=rwmech-20) *
- Drill press ![lflogo.svg](/lflogo.svg =10x)


![lflogo.svg](/lflogo.svg =10x) *Not required when buying [LayerFused X301 Complete Kit](https://shop.layerfused.com/products/layerfused-x301-complete-kit)*
\* LayerFused is an Amazon Associates and will earn a commission at no additional cost to you if you use this link