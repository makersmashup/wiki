---
title: Troubleshoot - General
description: 
published: true
date: 2022-10-04T17:28:03.174Z
tags: general, troubleshoot
editor: markdown
---

This section contains infomation about troubleshooting 3D Printers in general.

# Components

- [Stepper Motors](stepper-motors)

# CoreXY

- [CoreXY Motion](corexy-motion)
- [CoreXY Gantry Squaring](gantry-squaring)

# Firmware

- [Marlin: "Err: EEPROM Version"](/general/firmware/marlin/eeprom-error)

# Printing

- [Printing](printing)
- [Calibration](calibration)
