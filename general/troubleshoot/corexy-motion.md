---
title: CoreXY Motion
description: 
published: true
date: 2022-10-03T23:55:21.697Z
tags: 
editor: markdown
---

> This guide is a work in progress.
{.is-warning}

The first time you power on your printer you may find that your axes do not move as you'd expect.  This guide will take you step by step through identifying your issue, what to check and how to correct it.  In some cases there will be multiple methods to correct the problem, but we will hightlight the preferred option.

Before we get started, please review the following diagrams.  These will clarify the different components as well as their behavior when moving the axes.  Compare these to what is occuring with your printer and proceed to [Identifying your problem](#identifying-your-problem).

Note that all of the below troubleshooting assumes you are using the latest version of the LayerFused X301 Marlin firmware which can be found [here](https://bitbucket.org/makersmashup/marlin/src/2.0.x-x301/)

# Identifying your problem

Here are the typical issues we see along with which step to go to next for each problem:

- One or more steppers do not move or only jitter when attempting to move them
-- Go to [Checking your wiring/connections](#check-your-wiringconnections)
- Attemping to move an axis moves the opposite axis or an axis is moving in the wrong direction
-- Go to [Checking your wiring/connections](#check-your-wiringconnections)
- Axis moves less or more than commanded
-- Go to [Check stepper specifications](#check-stepper-specifications)
- Homing does not move carriage to front left corner
-- Go to [Diagnosing homing issues](#diagnosing-homing-issues)


# Checking your wiring/connections
TODO

# Checking stepper specifications
TODO

# Diagnosing Homing Issues
TODO