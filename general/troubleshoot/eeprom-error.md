---
title: EEProm Version Error
description: 
published: true
date: 2022-10-04T00:08:33.653Z
tags: moved
editor: markdown
---

This page has been moved [here](/general/firmware/marlin/eeprom-error).