---
title: Printing
description: 
published: true
date: 2022-10-03T23:44:37.032Z
tags: 
editor: markdown
---

# Print Troubleshooting
This contains frequent issues with new 3D printer builds. 

## Under Extrusion
If your printer is under extruding this is generally caused by two things.  Incorrect steps per mm settings in the firmware or a misconfiguration of the slicer.  

### Steps Per mm
If you are using the LayerFused firmware and the appropriate extruder from the bill of materials your printer should be properly calibrated for the proper steps per mm.  For a geared extruder or "BMG" style this is typically 415.  Adjustments do not typically need to be made to this.  If you are using another type of geared extruder consult the manufacturer documentation for their recommended settings.  

### Slicer Miconfiguration
The very first thing to check is that your slicer is setup to use 1.75mm filament.  Cura has a default of 2.85 which will result in massive under extrusion when using the LayerFused default 1.75 filament setup.  In Cura this setting is under the Machine settings under the nozzle tab. 

Also check your slicer to ensure that your flow compensation is at 100%.  While tuning a 3D Printer you should have your flow set at 100%.

Check to make sure you're using the correct nozzle size.  Using a .8 nozzle with a setting for a .4 printer will also cause massive under extrusion as half the volume will be flowing through the nozzle. 