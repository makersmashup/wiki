---
title: Stepper Motors
description: 
published: true
date: 2022-10-03T23:42:43.168Z
tags: 
editor: markdown
---

# Stepper Motors
This page contains content related to Stepper motors and LayerFused printers. 

## Steppers Temperature
Stepper motors by their very nature produce some heat.  The key is to maintain the stepper temperature below 40c. This can be done a number of ways. 
### Current
The stepper current should be enough to prevent layer shifts and missed steps but low enough that your current is not causing the stepper to heat up.  Start with your stepper specifications and begin at 50%-75% of its rated capacity.  For example if your stepper supports 1A of current start with 500ma to 750ma as the starting point for your stepper current.  Use the lowest value and increase if you see problems with skipped steps. 

## Turning wrong direction
If your steppers are turning the wrong direction it is usually due to wiring. Some wires & steppers are wired with different connections to the internal coils. It is always good to reference the vendors wiring diagram when installing steppers.  

Should your steppers be turning in the wrong direction you can solve this easily in the Marlin Firmware. Simply modify the configuration.h of marlin and the following lines.
```
// Invert the stepper direction. Change (or reverse the motor connector) if an axis goes the wrong way.
#define INVERT_X_DIR false
#define INVERT_Y_DIR false
#define INVERT_Z_DIR false
```

After recompiling your firmware it should now be going the proper direction and no wiring changes were necessary. 

## Grinding noise, wobbling
If your steppers (without any belts on it) are vibrating back and forth, not turning, making a grinding noise or wobbling instead of moving smoothly the most likely cause is your wiring.  Some wires & steppers are wired with different connections to the internal coils. It is always good to reference the vendors wiring diagram when installing steppers.