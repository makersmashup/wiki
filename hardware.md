---
title: LayerFused Hardware
description: 
published: true
date: 2022-10-03T20:28:46.674Z
tags: 
editor: markdown
---

# LayerFused Hardware

> This page does not contain everything available from the Layerfused shop. Things are still being added.
{.is-warning}


The following guides provide assembly instructions as well as troubleshooting guidance for all LayerFused electronics kits.

[Lighted Bed Kit](lighted-bed-kit)