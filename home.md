---
title: Home
description: 
published: true
date: 2022-10-07T02:04:01.134Z
tags: 
editor: markdown
---

> The easiest way to use this wiki is to use the search at the top of the page.  Simply search for information you are looking for.
{.is-warning}

# LayerFused Wiki

Welcome to the LayerFused wiki.  You can find information on all of the LayerFused products as well as more general content related to 3D printing.

## [LayerFused Printers](printers)

We have dedicated sections in the wiki for each printer so you can find relevant information quickly.  Start with these sections if you're building or using a LayerFused printer.

Click the link above for a list of printers.

### [Mods](printers/mods)

Given the Open Source nature of the LayerFused 3D Printers, many members of the community have highly modified their printers from the original designs. If you are looking to get started building your first LayerFused printer, definitely check some of these mods out before you buy anything in case you want to use some mods that change your orders.

Click the link above to see a list of mods and creators.

### [Builds](printers/builds)

Given that this is a highly modifiable and DIY printer, it is often the case that each is a little unique. It is also fun to show off what you have made after putting so many hours into it.

Click the link above to see a list of LayerFused 3D Printers made by some of our community members.

## [LayerFused Hardware](hardware)

You can find assembly instructions and troubleshooting information for all LayerFused hardware.

Click the link above for a list of LayerFused Hardware.

## [General Information](general)

Information in this section will help with 3D Printing in general.

Click the link above for a list of general support articles

## [Community](community)

If you like the work we are doing, consider joining our [Community](community) to keep up with the latest developments and contribute.