---
title: LayerFused Printers
description: 
published: true
date: 2022-10-10T16:50:53.191Z
tags: 
editor: markdown
---

# LayerFused Printers

We have dedicated sections in the wiki for each printer so you can find relevant information quickly.  Start with these sections if you're building or using a LayerFused printer.

- [C201](c201)
- [X201](x201)
- [X301](x301)
- [X501](x501)

## [Mods](mods)

Given the Open Source nature of the LayerFused 3D Printers, many members of the community have highly modified their printers from the original designs. If you are looking to get started building your first LayerFused printer, definitely check some of these mods out before you buy anything in case you want to use some mods that change your orders.

Click the link above to see a list of mods and creators.