---
title: Bill of Materials - bamhm182's X301
description: 
published: true
date: 2022-10-07T18:46:40.903Z
tags: 
editor: markdown
---

### Frame - $266.33

| Part | Count | Cost | Total | Source
| --- | --- | --- | --- | ---
| HFS5-2080-400 | 3 | $12.48 | $37.44 | [MiSUMi](https://us.misumi-ec.com/vona2/detail/110302684870/?ProductCode=HFS5-2080-400)
| HFS5-2020-400 | 7 | $5.80 | $40.60 | [MiSUMi](https://us.misumi-ec.com/vona2/detail/110302683830/?ProductCode=HFS5-2020-400)
| HFS5-2020-300 | 3 | $4.35 | $13.05 | [MiSUMi](https://us.misumi-ec.com/vona2/detail/110302683830/?ProductCode=HFS5-2020-300)
| HFS5-2040-400 | 2 | $6.76 | $13.52 | [MiSUMi](https://us.misumi-ec.com/vona2/detail/110302684350/?ProductCode=HFS5-2040-400)
| 300mm T Slot 2020 (4x) | 1 | $17.99 | $17.99 | [Amazon](https://www.amazon.com/dp/B08744RN57/?ref=nosim&tag=rwmech-20) *
| 2020 L Corner Brackets (20x) | 1 | $8.99 | $8.99 | [Amazon](https://www.amazon.com/dp/B07GGLYX9V/?ref=nosim&tag=rwmech-20) *
| 2020 T Slot Brackets (10x) | 1 | $7.99 | $7.99 | [Amazon](https://www.amazon.com/dp/B01GZYK84C/?ref=nosim&tag=rwmech-20) *
| 2020 L Join Plates (4x) | 3 | $9.99 | $29.97 | [Amazon](https://www.amazon.com/dp/B07PN87SXK/?ref=nosim&tag=rwmech-20) *
| X301 Base Plate | 1 | $41.99 | $41.99 | [LayerFused](https://shop.layerfused.com/products/x301-aluminum-base-plate)
| X301 Aluminum Carriage Plate | 1 | $14.99 | $14.99 | [LayerFused](https://shop.layerfused.com/products/x301-aluminum-carriage-plate)

After putting in the order with MiSUMi, I saw the [6+2 Point Heated Bed Frame Mod](/printers/mods/johanmalay/8-point-bed-mount) and put in an order for more extrusions on Amazon rather than paying shipping again with MiSUMi. I should have realized this before putting in my order and just added 2 more HFS5-2020-300. With all parts listed and $18.81 of shipping from MiSUMi, I paid $263.33 for the frame.

### Movement - $129.95

| Part | Count | Cost | Total | Source
| --- | --- | --- | --- | ---
| 400mm Linear Rails | 5 | $25.99 | $129.95 | [Amazon](https://www.amazon.com/dp/B07ZVFTZVP/?ref=nosim&tag=rwmech-20) *

I originally wasn't paying close enough attention and Amazon linked to MGN9C rails because they were out of the intended MGN12H rails. Double and tripple check that you are buying the correct rails. For a quick breakdown, the 12 refers to the rails being 12mm wide and the H refers to the carriage type. These both need to be accurate.

### Electronics - $250.09

| Part | Count | Cost | Total | Source
| --- | --- | --- | --- | ---
| 10A Panel Sockets (6x) | 1 | $8.34 | $8.34 | [Amazon](https://www.amazon.com/dp/B07F2XNFRV/?ref=nosim&tag=rwmech-20) *
| Mean Well SE-600-24 PSU | 1 | $82.00 | $82.00 | [Amazon](https://www.amazon.com/dp/B07PGYHYV8/?ref=nosim&tag=rwmech-20) *
| 24V 60W Heater Cartridge | 1 | $7.99 | $7.99 | [Amazon](https://www.amazon.com/dp/B08CNCHC29/?ref=nosim&tag=rwmech-20) *
| BTT EBB42 Canbus Toolhead | 1 | $39.99 | $39.99 | [Amazon](https://www.amazon.com/dp/B0B1MFXGTZ/?ref=nosim&tag=rwmech-20) *
| BTT U2C Adapter Board | 1 | $25.99 | $25.99 | [Amazon](https://www.amazon.com/dp/B0B1X47319/?ref=nosim&tag=rwmech-20) *
| 30A Heatbed Mosfet (2x) | 1 | $11.99 | $11.99 | [Amazon](https://www.amazon.com/dp/B07C7RQ4C6/?ref=nosim&tag=rwmech-20) *
| BTT BL Touch | 1 | $44.51 | $44.51 | [Amazon](https://www.amazon.com/dp/B08J9WN356/?ref=nosim&tag=rwmech-20) *
| BTT Manta M8P/CB1/8 TMC2209 | 1 | $104.58 | $104.58 | [BIQU](https://biqu.equipment/products/manta-m4p-m8p)
| 300mm Heat Bed | 1 | $41.39 | $41.39 | [AliExpress](https://www.aliexpress.us/item/2251832695087916.html)
| Lighted Bed Kit | 1 | $9.99 | $9.99 |	[LayerFused](https://shop.layerfused.com/products/neopixel-lighted-bed-kit)
| Igus Chainflex CF5 - 4 Core 20AWG (10ft) | 1 | $19.29 | $19.29 | [Igus](https://www.igus.com/product/894)

Somehow I missed that the LayerFused shop sells the heated bed both [alone](https://shop.layerfused.com/collections/all-products/products/mk2a-heated-bed-24v-300x300-mm) and [with a mosfet](https://shop.layerfused.com/collections/all-products/products/x301-heated-bed-kit-mk2a-300mmx300mm-24v-with-mosfet). I absolutely should have bought directly from them.

### Misc - $99.35

| Part | Count | Cost | Total | Source
| --- | --- | --- | --- | ---
| PEI Build Plate | 1 | $44.99 | $44.99 | [LayerFused](https://shop.layerfused.com/products/double-sided-mk2a-pei-build-plates)
| Heatbed Leveling Columns (4x) | 2 | $10.69 | $21.38 | [Amazon](https://www.amazon.com/dp/B07S2R5C6P/?ref=nosim&tag=rwmech-20) *
| Aluminum Carriage Plate | 1 | $14.99 | $14.99 | [LayerFused](https://shop.layerfused.com/products/x301-aluminum-carriage-plate)
| X301 Electronics Cover | 1 | $25.99 | $25.99 | [LayerFused](https://shop.layerfused.com/products/x301-acrylic-electronics-cover)


### Printed Parts - $41.98

| Part | Count | Cost | Total | Source
| --- | --- | --- | --- | ---
| TPU Feet | 1 | $6.99 | $6.99 | [LayerFused](https://shop.layerfused.com/products/x301-aluminum-carriage-plate)
| 1kg PETG - Black | 2 | $20.99 | $41.98 | [Amazon](https://www.amazon.com/dp/B07PGYHYV8/?ref=nosim&tag=rwmech-20) *


$6.99 to not have to fuss with TPU seemed worth it to me. If you have TPU dialed in, you might consider printing these yourself.

### Not Included

There are a number of expenses that you would have which I did not purchase for this project. Here is a list of those.

- Stepper Motors
- Hotend (E3D Volcano)
- Misc nuts and screws
- Misc connectors, wire, tools
- Rod/Rail Lubricant
- Extruder (BondTech QR)

\* LayerFused is an Amazon Associates and will earn a commission at no additional cost to you if you use this link