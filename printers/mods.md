---
title: Mods
description: 
published: true
date: 2022-11-08T17:28:08.120Z
tags: 
editor: markdown
---

Given the Open Source nature of the LayerFused 3D Printers, many members of the community have highly modified their printers from the original designs. If you are looking to get started building your first LayerFused printer, definitely check some of these mods out before you buy anything in case you want to use some mods that change your orders.

You can find mods pages detailing individual mods contributed by members of our community below.

- [Bwnance](bwnance): Pillow Blocks, 3 Z Magnetic Bed
- [bamhm182](bamhm182): BTT EBB42
- [Johanmalay](johanmalay): Over 25 Mods which range from major strength and accuracy improvements to accessory mounts
- [Senshado](senshado): AC Heatbed
- [Unknown](unknown): Dual-Z Axis Configuration