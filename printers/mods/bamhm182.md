---
title: bamhm182's Mods
description: 
published: true
date: 2022-11-08T17:31:43.964Z
tags: 
editor: markdown
---

This is a list of mods created by bamhm182.

# License
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0). To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/

# Mod-List

## [BTT EBB42](btt-ebb42)

The BigTreeTech EBB42 (and 36) boards allow you to connect everything that exists on your carriage back to your electronics box with only 4 cables. This means that instead of having approximately 15-25 wires making the trip back to the electronics box, they only need to travel a couple of inches, and you can have 4 cables travel the longer distance.