---
title: BTT EBB42 - bamhm182 Mods
description: 
published: true
date: 2024-07-29T07:06:05.459Z
tags: 
editor: markdown
---

# Overview

The BigTreeTech EBB42 (and 36) boards allow you to connect everything that exists on your carriage back to your electronics box with only 4 cables. This means that instead of having approximately 15-25 wires making the trip back to the electronics box, they only need to travel a couple of inches, and you can have 4 cables travel the longer distance.

# Hardware

## Purchased Parts

* (1) [4-core 20-AWG Igus ChainFlex](https://www.igus.com/product?artNr=CF5-05-04) (It doesn't need to be this, but this cable seemed great. The length will vary depending on your setup and routing.)
* (1) [BTT EBB-42](https://biqu.equipment/products/bigtreetech-ebb-36-42-can-bus-for-connecting-klipper-expansion-device?variant=39760665182306)
* (1) [BTT U2C](https://biqu.equipment/products/bigtreetech-ebb-36-42-can-bus-for-connecting-klipper-expansion-device?variant=39762747949154) (Recommended, but may not be necessary if you have other means to connect your BTT EBB-42 to your Klipper Host)
* (8) M3 Heat Set Nuts
* (3) M5 8mm Screw
* (3) M5 t-nut

## Printed Parts

- (1) [EBB-42 Mount](https://gitlab.com/bytepen/3d-printing/layerfused-x301/-/blob/master/btt-ebb-mount/btt-ebb-42-mount.stl)
- (1) Flexchain bracket (haven't created this yet, using zip ties for the time being)
- (1) U2C Mount (haven't created this yet)

## Installation

## Software

The official BTT documentation isn't super clear, and has you unnecessarily use STM32CubeProgrammer, which can be a hassle and isn't necessary. I will walk through the installation of software using only your Klipper host.

### Install CanBoot onto the EBB

You can build CanBoot with the following steps:

```
cd ~/
git clone https://github.com/Arksine/CanBoot.git
cd CanBoot
make menuconfig
```

Configure the following settings. If you have any doubts, cross reference the BTT EBB documentation.

Micro-controller Architecture: `STMicroelectronics STM32`
Processor Model: `STM32G0B1`
Build application: `Do not build`
Clock Reference: `8MHz Crystal`
Communication interface: `CAN bus (on PB0/PB1)`
Application start offset: `8KiB offset`
CAN bus speed: `250000`

> If you're having trouble, try messing with the CAN bus speed. I personally had luck around 1,000,000 for everything.
{.is-warning}


Run the following commands to make the firmware:

```
cd ~/CanBoot
make clean
make
```

Ensure that the U2C is not connected to the EBB and jump the USB-Power pins. Connect the EBB to your Klipper Host via USB. There are two buttons on the EBB. There is nothing on the board that signals you are in DFU mode, but run the following command to verify that the EBB is in DFU mode:

```
$ lsusb
Bus 002 Device 005: ID 0483:df11 STMicroelectronics STM Device in DFU Mode
```

Execute the following command to flash the firmware, making sure that `d` matches the ID in the `lsusb` command.

```
sudo dfu-util -a 0 -D ~/CanBoot/out/canboot.bin --dfuse-address 0x08000000:force:mass-erase:leave -d 0483:df11
```

Ignore any errors so long as you see a line stating `File downloaded sucessfully`

Power everything off and remove the USB-power jumper.

### Install CandleLight onto the U2C

> You can use the original repository if you have a v1.x U2C. If you have the v2.x, you need to use the bigtreetech branch as shown below.
{.is-warning}


You can build CandleLight with the following steps:

```
sudo apt-get install cmake gcc-arm-none-eabi git
cd ~/

git clone https://github.com/candle-usb/candleLight_fw
cd candleLight_fw

# v2.1
# git clone https://github.com/bigtreetech/candleLight_fw
# cd candleLight_fw
# git checkout stm32g0_support

mkdir build
cd build
cmake .. -DCMAKE_TOOLCHAIN_FILE=../cmake/gcc-arm-none-eabi-8-2019-q3-update.cmake
make
```

Ensure your U2C is **unplugged** from your Klipper host, then hold down the boot button at the same time you are plugging it in. You should see a blue and a green light on the U2C.

Run the following command and confirm you have something very similar in the output:

```
$ lsusb
Bus 002 Device 006: ID 0483:df11 STMicroelectronics STM Device in DFU Mode
```

If you do, then flash CandleLight onto the U2C with the following command:

```
cd ~/candleLight_fw/build
sudo make flash-candleLight_fw
```

Any error messages that appear don't matter so long as you see a line stating `File downloaded sucessfully`. Unplug your U2C and plug it back in. You should no longer see the blue light.

Create a file at `/etc/network/interfaces.d/can0` with the following contents:

```
auto can0
iface can0 can static
  bitrate 250000
  up ifconfig $IFACE txqueuelen 1024
```

Reboot your Klipper host and run the following commands while verifying you have something similar to their output:

```
$ ifconfig
can0: flags=193<UP,RUNNING,NOARP>  mtu 16
        unspec 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00  txqueuelen 1024  (UNSPEC)
        RX packets 29530  bytes 236240 (230.7 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 3  bytes 24 (24.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

$ ~/klippy-env/bin/python ~/klipper/scripts/canbus_query.py can0
```

### Flash Klipper firmware to the EBB

Start off by connecting everything as described in the Physical section below.

Execute the following commands to build the klipper firmware for the EBB:

```
cd ~/klipper
make menuconfig
```

Set the following options:

* Enable extra low-level configuration options: `Checked`
* Micro-controller Architecture: `STMicroelectronics STM32`
* Processor model: `STM32G0B1`
* Bootloader offset: `8KiB bootloader`
* Clock Reference: `8 MHz Crystal`
* Communication interface: `CAN bus (on PB0/PB1)`
* CAN bus speed: `250000`

Execute the following commands to build the firmware:

```
cd ~/klipper
make clean
make
```

Get your `canbus_uuid` as seen in the following command and output:

```
$ ~/klippy-env/bin/python ~/klipper/scripts/canbus_query.py can0
Found canbus_uuid=0123456789ab, Application: CanBoot
Total 1 uuids found
```

Then flash Klipper with the following command, making sure to replace my uuid with your uuid:

```
python3 ~/CanBoot/scripts/flash_can.py -i can0 -f ~/klipper/out/klipper.bin -u 0123456789ab
```

You should see a message indicating `CAN Flash Success`.

Run the canbus_query again and ensure that Application has changed to `Klipper` as seen below:

```
~/klippy-env/bin/python ~/klipper/scripts/canbus_query.py can0
Found canbus_uuid=0123456789ab, Application: Klipper
Total 1 uuids found
```

### Configure Klipper

Add the following to your `printer.cfg`

```
[mcu ebb42]            
canbus_uuid: 0123456789ab

[board_pins ebb42]
mcu: ebb42
aliases:
  # ADXL345
  CAN_ADXL_CS=PB12, CAN_ADXL_SCLK=PB10, CAN_ADXL_MISO=PB2, CAN_ADXL_MOSI=PB11,
  # TMC2209
  CAN_M1_STEP=PD0, CAN_M1_DIR=PD1, CAN_M1_EN=PD2, CAN_M1_UART=PA15,
  CAN_H0=PB13, CAN_T0=PA3,
  CAN_F0=PA0, CAN_F1=PA1,
  CAN_BLT_SENSOR=PB8, CAN_BLT_CTL=PB9
```

Then you can go through your printer and update the pins as appropriate depending on what you are moving to the EBB42. For example, this is my new `bltouch` section:

```
[bltouch]
sensor_pin: ebb42: CAN_BLT_SENSOR
control_pin: ebb42: CAN_BLT_CTL
x_offset: 21.26
y_offset: 6
```

If you're having trouble, you can see my running Klipper config [here](https://gitlab.com/bytepen/3d-printing/x301-klipper-config).

## Physical

I'll do better on this part in the future.

## U2C

The U2C has 4 screw terminals. Two of them are for 12/24V in and the other two are for can communication. Once done flashing, you will connect the Vin pins to your PSU. The documentation is subpar for determining which of the EBB-42 connector pins go where.

Double check with a multimeter. I will add a picture later, but here are the outputs if you are looking head on at the pins with the tab on top:

## EBB-42