---
title: Bwnance - Mods
description: 
published: true
date: 2022-10-24T13:54:23.378Z
tags: 
editor: markdown
---

This is a list of mods created by Bwnance. These mods may be modified versions of original parts by the LayerFused team or original work to add to the printers.

# License
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0). To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/

# Mod-List

[MakersMashup Mod Repo](https://bitbucket.org/makersmashup/layerfused-x-series-mods/src/main/)

## [Pillow Blocks](pillow-blocks)

The pillow blocks mod is an adaptation of the RatRig Pillow Blocks seen [here](https://ratrig.dozuki.com/Guide/04.+Z+motor+mounts/71). These pillow blocks sit on top of the existing motor mounts in order to shift Z-Axis stress from the shaft to the body of the Z-Axis Stepper Motor.

## [3 Z Magnetic Bed](3-z-magnetic-bed)

There is a BOM list with all of the extra hardware that you will need to install this mod.  The parts are mostly on McMaster Carr.  You can pick everything up for around $50 USD.  

Insert Pictures here!