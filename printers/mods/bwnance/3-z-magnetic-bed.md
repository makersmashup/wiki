---
title: 3 Z Magnetic Bed - Bwnance Mods
description: 
published: true
date: 2022-10-24T14:21:13.615Z
tags: 
editor: markdown
---

# Overview

Coming soon...

[MakersMashup Mod Repo](https://bitbucket.org/makersmashup/layerfused-x-series-mods/src/main/)

# Hardware

## Purchased Parts

There is a [BOM list](https://bitbucket.org/makersmashup/layerfused-x-series-mods/raw/50abd2ce956a0f4c7dbb025fea79d2ef4608b476/Bwnance%203Z%20BOM.pdf) with all of the extra hardware that you will need to install this mod. It has been copied here for convenience, but double check they are the same before making purchases. The parts are mostly on McMaster Carr with a couple on Amazon. As of OCT2022, the McMaster Carr cart costs $66.33 + Shipping and the Amazon cart costs $18.78, bringing the total to just under $100.

- (3) [Neodymium Magnet with Countersunk Holes on Both Sides, 1/4" x 1/2" x 1"](https://www.mcmaster.com/5862K246)
- (1) [316 Stainless Steel Hex Drive Flat Head Screw 82 Degree Countersink Angle, 6-40 Thread Size, 1/2" Long - Pack of 100](https://www.mcmaster.com/90585A133/)
- (1) [18-8 Stainless Steel Hex Nut 6-40 Thread Size - Pack of 100](https://www.mcmaster.com/91841A175/)
- (1) [Dowel Pin 52100 Alloy Steel, 3 mm Diameter, 40 mm Long - Pack of 25](https://www.mcmaster.com/91595A142/)
- (1) [Bolsen 16pcs/Set 10mm M4 Threaded Steel Ball Rod Ends](https://www.amazon.com/dp/B07HQ72H1R/?ref=nosim&tag=rwmech-20)*
- (1) [Atoplee 4pcs Miniature Axial Ball Thrust Bearings F8-16M (8x16x5mm)](https://www.amazon.com/dp/B0192SOXN0/?ref=nosim&tag=rwmech-20)*

\* LayerFused is an Amazon Associates and will earn a commission at no additional cost to you if you use this link

## Printed Parts

- (1) [Left Bedmount](https://bitbucket.org/makersmashup/layerfused-x-series-mods/src/main/x301_mag_left_bedmount.stl)
- (1) [Left Socket](https://bitbucket.org/makersmashup/layerfused-x-series-mods/src/main/x301_mag_left_socket.stl)
- (1) [Rear Bedmount](https://bitbucket.org/makersmashup/layerfused-x-series-mods/src/main/x301_mag_rear_bedmount.stl)
- (1) [Rear Socket](https://bitbucket.org/makersmashup/layerfused-x-series-mods/src/main/x301_mag_rear_socket.stl)
- (1) [Right Bedmount](https://bitbucket.org/makersmashup/layerfused-x-series-mods/src/main/x301_mag_right_bedmount.stl)
- (1) [Right Socket](https://bitbucket.org/makersmashup/layerfused-x-series-mods/src/main/x301_mag_right_socket.stl)

# Instructions

Coming soon...