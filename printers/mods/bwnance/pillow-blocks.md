---
title: Pillow Blocks
description: 
published: true
date: 2022-10-24T15:10:54.378Z
tags: 
editor: markdown
---

# Overview

The pillow blocks mod is an adaptation of the RatRig Pillow Blocks seen [here](https://ratrig.dozuki.com/Guide/04.+Z+motor+mounts/71). These pillow blocks sit on top of the existing motor mounts in order to shift Z-Axis stress from the shaft to the body of the Z-Axis Stepper Motor.

# Hardware

## Purchased Parts

- (8) [20mm M3 Socket Head Screws](https://www.amazon.com/dp/B0967ZK22Q/?ref=nosim&tag=rwmech-20)* (Replaces the 12mm screws attaching the Z motor mounts to the steppers)
- (2) [Thrust Ball Bearings](https://www.amazon.com/dp/B07QLTXJDH/?ref=nosim&tag=rwmech-20)* (Bore Diameter=8mm, Outer Diameter=16mm, Thickness=5mm)

\* LayerFused is an Amazon Associates and will earn a commission at no additional cost to you if you use this link

## Printed Parts

- (2) [Pillow Blocks](https://bitbucket.org/makersmashup/layerfused-x-series-mods/src/main/pillow_block_lowered.stl)

# Instruction

> Your bed is going to want to come down while you are performing this mod. Try to keep the bed as level as possible to avoid bending anything.
>
> Alternatively, install this mod as part of your initial build before you put the bed on.
{.is-warning}


Instructions are per z-axis stepper motor. They will need to be repeated twice for a standard X301 and thrice if you have completed the 3 Z-Axis Mod.

Print (1) Pillow Block. Gather (4) 20mm M3 Socket Head Screws and (1) Thrust Ball Bearing.

[Insert picture of required items]

Loosen the flexible coupler and (4) 8mm M3 screws on your stepper motor so the motor drops down.

[Insert picture of dropped down motor]

Insert (1) thrust bearing into the 3D Printed pillow block and slide it above the motor bracket, then screw everything into place with the (4) 20mm M3 screws.

Note that the thrust bearing washers have an indented and a flat side. The bearings should come in contact with the indented side.

[Insert picture showing thrust bearing and screws]
[Insert picture of mod complete]