---
title: 24v to USB Converter Mount - Johanmalay Mods
description: 
published: true
date: 2022-10-04T16:37:42.336Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/24v%20to%20USB%20Converter%20Mount/
{.is-info}

A simple mount for this 24v to USB Converter I got from Amazon: https://www.amazon.com/gp/product/B01HM12N2C

I use this converter to safely power my Raspberry Pi off my printer's 24v PSU. Use 3M VHB double sided tape to attach it to the electronics box.

# Example Image
![24v to USB Converter Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/e0e5e7684134d5e6013b67a641dc4ec668efeecd/24v%20to%20USB%20Converter%20Mount/model.png)