---
title: 60mm Fan Extrusion Mount - Johanmalay Mods
description: 
published: true
date: 2022-10-04T16:39:08.799Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/60mm%20Fan%20Extrusion%20Mount/
{.is-info}

Useful for mounting 60mm x 10mm fans inside the X301 Electronics box. Can be mounted either vertically (blowing across electronics) or horizontally (blowing down onto electronics).

The bracket is connected to the side of the 8020 extrusion with M5 socket head cap screws and M5 t-nuts. The cap screws are countersunk inside the bracket so the fan can sit inside the shape of the frame, covering the M5 screws. This creates a very rigid shape.

The fan is connected to the bracket with M4 x 12mm socket head cap screws.

There are two versions included and they require different length screws for mounting to the extrusion:

* The standard one uses M5 x 8mm screws.
* The 15mm extended one uses M5 x 20mm screws.

# Example Images
## Normal Mount
![60mm x 10mm Fan Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/8c222bf835fef9d5a9d83cef2236b433454044ae/60mm%20Fan%20Extrusion%20Mount/model.png)

## 15mm Extension
![60mm x 10mm Fan Mount - 15mm Extension](https://bitbucket.org/malhan-designs/x301-mods/raw/8c222bf835fef9d5a9d83cef2236b433454044ae/60mm%20Fan%20Extrusion%20Mount/model-extended.png)