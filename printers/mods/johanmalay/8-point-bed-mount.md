---
title: 6+2 Point Heated Bed Frame - Johanmalay Mods
description: 
published: true
date: 2022-10-24T16:02:36.973Z
tags: 
editor: markdown
---

# Overview

> PLEASE NOTE: The STL files in this folder are only compatible with 2 Z screw x301 builds. If you are building a 3 Z model, please use the STL files for the bed that are included in the 3 Z folder. 
{.is-danger}

This mod changes the headed bed frame from a 4 point mount to a 6 point mount. Additionally, you can add 2 more mounting points to the existing frame pieces for a total of 8 points with the right parts.

# Hardware

This section assumes you have a fully assembled X301 and will list the parts required to upgrade a complete printer.

## Purchased Parts

- (2) 300mm 2020 Aluminum Extrusions (total of 5, including the 3 from the stock bed)
- (8) [2020 Corner Brackets](https://www.amazon.com/dp/B07JGNLL27/?ref=nosim&tag=rwmech-20) (You can choose to use 4 if you use the standard bed mounting brackets)
- (4) Silicon Bed Mounts (total of 8, including the 4 from the stock bed)
- (X) 8mm M5 Screws (total of 20, including the X from the stock bed)
- (X) M5 t-nuts (total of 20, including X from the stock bed)
- (32) 10mm M4 Screws (amount varies based on the number of corner brackets used)
- (32) M4 t-nuts (amount varies based on the number of corner brackets used)

---
Original Page
---


# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/6%20%2B%202%20Point%20Heated%20Bed%20Frame/
{.is-info}



Derived from: 

* https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/Bed%20Mount%20Lighted.f3d
* https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/Bed%20Mount.f3d

# Hardware Required

## For the 6 Point Mount

Note: some hardware can be reused from the standard X301 build if desired.

* 5x 300mm 2020 Aluminum Extrusion (reuse the 3 from the base X301 build, so only 2x additional are needed) - This adds the front and rear support beams for the new central mounts
* 8x 2020 corner brackets - https://www.amazon.com/gp/product/B07JGNLL27 (alternatively you can omit the central 4 of these and use the layer fused printed brackets from the standard bed)
* 6x silicon mounts from the x301 BOM (these come in packs of 4, and you'll need 8 total to do all 8 points, if you already have 4, just get 1 more pack)
* 20x M5 x 8mm cap screws
* 20x M5 t-nuts
* 32x M4 x 10mm button head screws (amount varies based on how many 2020 corner brackets you use)
* 32x M4 t-nuts (amount varies based on how many 2020 corner brackets you use)

**Your choice of:**

* 6x M3 x 35mm countersunk screws
* 6x M3 nuts
or
* 6x M3 x 40mm countersunk screws
* 6x M3 wingnuts

## For the +2 Point Mounts

* 2x silicon mounts from the x301 BOM
* 2x M3 x 16mm countersunk screws
* 2x M3 t-nuts

# Build Notes

Included below are some images from Fusion 360 of how I put this together on my build. If you don't have a lighted bed, instead of printing the **Front Side Mounts**, just print the 2x of the **Rear Side Mounts**.

This design includes a new solution for the bed wiring strain relief. It is integrated into the rear mount. There is a slot in the strain relief for a small zip tie so you can tighten the wires down into place.

I found that I needed to desolder and change the angle of the positive lead on the heated bed to get clearance for the rear silicon mount. The heated bed acts as a heat sink when attempting to desolder these wires, a high wattage soldering gun is highly recommended.

Use the corner brackets with M4 hardware to build the base frame from the 5 pieces of extrusion, reusing the 3 that make up the standard X301 bed frame. Attach the new mounts with the M5 hardware. Insert the silicon mounts and use the M3 hardware of your choice to mount the bed to the frame.

To add the central side mounts, take your extra 2 silcon mounts, and modify them so they look like the picture below. They cut easily with a box knife. Trim them until they are about the same height as the mounts in the printed parts when placed in the channel of the extrusion. To make the side mounts work, you place an M3 t-nut in the extrusion channel, rotate it into the locked position, then push the silicon mount over the top of it. This will keep the t-nut in the locked position and allow you to screw the M3 x 16mm countersunk screw into it to tighten down the bed. This allows you to tune the tension on the central sides of the bed.

# Example Images

## Completed 6 + 2 Point Heated Bed

![6 + 2 Point Heated Bed - Bare frame](https://bitbucket.org/malhan-designs/x301-mods/raw/e92300be6599f5712bef30cb8e0dba82592635cb/6%20%2B%202%20Point%20Heated%20Bed%20Frame/composite-model-c.png)
![6 + 2 Point Heated Bed - Top](https://bitbucket.org/malhan-designs/x301-mods/raw/27056387fbc26b78d0146fc1d2e7b1a2b4009e96/6%20%2B%202%20Point%20Heated%20Bed%20Frame/composite-model-a.png)
![6 + 2 Point Heated Bed - Bottom](https://bitbucket.org/malhan-designs/x301-mods/raw/27056387fbc26b78d0146fc1d2e7b1a2b4009e96/6%20%2B%202%20Point%20Heated%20Bed%20Frame/composite-model-b.png)

## Modified Silicon Mount

![Modified Silicon Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/27056387fbc26b78d0146fc1d2e7b1a2b4009e96/6%20%2B%202%20Point%20Heated%20Bed%20Frame/modified-silicon-mount.png)

## Printed Models

![Front Side Mounts](https://bitbucket.org/malhan-designs/x301-mods/raw/27056387fbc26b78d0146fc1d2e7b1a2b4009e96/6%20%2B%202%20Point%20Heated%20Bed%20Frame/model-front-sides.png)
![Rear Side Mounts](https://bitbucket.org/malhan-designs/x301-mods/raw/27056387fbc26b78d0146fc1d2e7b1a2b4009e96/6%20%2B%202%20Point%20Heated%20Bed%20Frame/model-rear-sides.png)
![Front Center Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/27056387fbc26b78d0146fc1d2e7b1a2b4009e96/6%20%2B%202%20Point%20Heated%20Bed%20Frame/model-front-center.png)
![Rear Center Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/27056387fbc26b78d0146fc1d2e7b1a2b4009e96/6%20%2B%202%20Point%20Heated%20Bed%20Frame/model-rear-center.png)