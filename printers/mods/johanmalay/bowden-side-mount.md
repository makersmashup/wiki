---
title: Bowden (Wiring) Side Mount - Johanmalay Mods
description: 
published: true
date: 2022-10-04T16:55:10.688Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/Bowden%20Mount%20-%20Side%20Wiring/
{.is-info}

This mount was designed to mount to the side of the 2020 extrusion that the Y-axis linear rails attach to. This shortens the filament path significantly, reducing friction. The mount includes additional considerations for wiring, should you need to run some along with the bowden tube.

Note that if you still run the main wiring for the carriage to the back of the printer, it will lose the support of the bowden tube with this mod.

# Hardware Required
2x M5 x 20mm screws (button or cap)
2x M5 t-nuts

# Example Image
![Bowden / Wiring Side Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/e0e5e7684134d5e6013b67a641dc4ec668efeecd/Bowden%20Mount%20-%20Side%20Wiring/model.png)