---
title: Extended Lighted Bed Mounts - Johanmalay Mods
description: 
published: true
date: 2022-10-04T16:01:40.069Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/Lighted%20Bed%20Mounts%20-%20Extended/
{.is-info}

> PLEASE NOTE: The STL files in this folder are only compatible with 2 Z screw x301 builds. If you are building a 3 Z model, please use the STL files for the bed that are included in the 3 Z folder. 
> {.is-danger}

Created for the **LayerFused Lighted Bed Kit**: https://shop.layerfused.com/collections/featured/products/neopixel-lighted-bed-kit?aff=2

Derived from: https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/Bed%20Mount%20Lighted.f3d

# Changes from Original Design

* Moves the LED strips up and away from the bed a bit to cast more light onto the bed and improve clearance from hotend.
* Increases the space on the inner back of the slot to allow the LED strip to be adhered more uniformly to the mount.
* Increases the diameter of the hole at the bottom of the mount for the wiring.

There are two versions, both increase the maximum elevation of the LEDs by 15mm:

* **Extended** - This one increases the overall height allowed for the LEDs by 15mm so you could use a longer strip than what's included in the kit.
* **Extended Raised** - This one raises the baseline for the LEDs, keeping the maximum length of the LED strip the same as what is included in the kit.

# Example Images
## Extended
![Lighted Bed Mounts - Extended](https://bitbucket.org/malhan-designs/x301-mods/raw/4b237e2f564ce5226f7b90a0960b4eb8944ddc7a/Lighted%20Bed%20Mounts%20-%20Extended/model.png)

## Extended & Raised
![Lighted Bed Mounts - Extended and Raised](https://bitbucket.org/malhan-designs/x301-mods/raw/4b237e2f564ce5226f7b90a0960b4eb8944ddc7a/Lighted%20Bed%20Mounts%20-%20Extended/model-raised.png)