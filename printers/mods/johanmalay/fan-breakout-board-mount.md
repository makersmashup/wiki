---
title: Fan Breakout Board Mount - Johan Mods
description: 
published: true
date: 2022-10-04T16:35:57.727Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/Fan%20Breakout%20Board%20Mount/
{.is-info}

A simple mount for the **LayerFused Dual Power 3d Printer Fan Breakout Board Kit**: https://shop.layerfused.com/collections/featured/products/dual-power-3d-printer-fan-breakout-board-24v-12v-5v?aff=2

I use 3M VHB double sided tape to attach it to the electronics box, so I didn't add any screw holes.

# Printing Notes
The tolerances on this part are very tight, so make sure you have your horizontal expansion and flow dialed in.

# Example Image
![Fan Breakout Board Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/682837506a4cee12fe7cd77d09e0fd3616979ce8/Fan%20Breakout%20Board%20Mount/model.png)