---
title: Lebula Raspberry Pi 7" IPS Touchscreen Mount - Johanmalay Mods
description: 
published: true
date: 2022-10-04T16:51:36.726Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/Lebula%20Pi%207in%20Touchscreen%20Mount/
{.is-info}

This is a portrait mount for the Lebula 7" Raspberry Pi IPS Touchscreen. This screen may be sold under other brand names, the key is that the resolution is advertised as 1024x600, it is IPS, capacitive, has speakers, and the Pi mounts to the back of it.

This mount is made to attach to the upper right side of the printer on a 3 Z screw setup. It uses the upper 2020 rail and the front 2040 rail for attachment points, which I've illustrated in the example image below.

# Additional Hardware
* Lebula 7" Touchscreen: https://www.amazon.com/gp/product/B07VNX4ZWY
* 4x M3 x 8mm screws
* 2x M5 x 10mm screws (button head recommended so you don't crack the print)
* 2x M5 t-nuts

# Printing Notes
* Print face down 
* Mind your speed and cooling for the overhangs

# Software Notes
As this is a portrait mount for this screen, you will need to rotate the output of your Raspberry Pi. Depending on which Pi and Raspbian OS, the procedure for doing this can vary. I found a lot of misinformation online about how to do this correctly. 

Here's what worked for me on a Raspberry Pi 4b running Raspbian OS Buster, as of the writing of this document. The screen output and the touch input are controlled separately on a Raspberry Pi 4b. 

**Prerequisites:**
* ssh into your pi
* Ensure you have x windows installed, if not: `sudo apt install raspberrypi-ui-mods`
* Ensure you have the screen configurator installed, if not: `sudo apt install arandr`. **Please note:** you need to install X windows _before_ running this command or arandr will not properly set up its hooks.
* I recommend setting up the pi to auto login, so enable that in `sudo raspi-config` under System Options -> Boot / Auto Login -> Desktop Autologin
* Raspi-config will ask you if you want to reboot when you're done, do so, and confirm that you are auto logged in to the Desktop.

**To rotate the screen output:**
* Edit / create the file `/usr/share/dispsetup.sh`
* Add the line: `xrandr --output HDMI-1 --rotate left` before the `exit 0` if it exists
* Ensure that `/usr/share/dispsetup.sh` is executable: `sudo chmod +x /usr/share/dispsetup.sh`
* Reboot
* Your screen output in X windows should now be rotated
* **Please note:** If this does not work or throws an error (likely to happen on pi4b), enable the GL driver in raspi-config
	* sudo raspi-config
	* Advanced Options
  * GL Driver
  * G2 GL (Fake KMS)
	* Finsh and Reboot

**To rotate the touchscreen input:**
* Edit or create the file `/etc/X11/xorg.conf.d/99-calibration.conf`. You may need to create the `/etc/X11/xorg.conf.d/` directory first
* Add the following to that file:
```
Section "InputClass"
        Identifier      "calibration"
        MatchProduct    "TSTP MTouch"
        Option  "TransformationMatrix"  "0 -1 1 1 0 0 0 0 1"
EndSection
```
* Reboot
* Your screen output and touch input should be rotated correctly and behave as expected.

From here, you'll want to setup the dashboard of your choosing on the pi in kiosk mode, whether it be for Octoprint, Mainsail, or Fluidd, etc.

**My steps for setting up Mainsail in kiosk mode (should work for Octoprint and/or fluidd as well):**
* Ensure chromium-browser is installed: `sudo apt install chromium-browser`
* Ensure unclutter is installed: `sudo apt install unclutter` (hides the mouse cursor)
* Ensure ratpoison is installed: `sudo apt install ratpoison` (this is an ultra light high performance display manager)
* Create the directory `.config/lxsession/LXDE-pi` if it doesn't exist
* Create / edit the file `.config/lxsession/LXDE-pi/autostart`
* Add the following to that file:
```
@xset s off
@xset s noblank
@xset -dpms

@unclutter &
ratpoison &
/usr/bin/chromium-browser --app=http://localhost --kiosk --noerrdialogs --overscroll-history-navigation=0 --disable-session-crashed-bubble --disable-infobars --check-for-update-interval=604800 --disable-pinch --enable-features=OverlayScrollbar,OverlayScrollbarFlashAfterAnyScrollUpdate,OverlayScrollbarFlashWhenMouseEnter
```
You can change the `--app=http://localhost` portion of the chromium-browser line to point to another url if needed.
* Reboot, and hopefully your pi should boot straight into your printer UI properly rotated and ready to go!


# Example Images
![Screen mount](https://bitbucket.org/malhan-designs/x301-mods/raw/e90183a9b56b5ecbf64f9ef5e59a6a5df9950ec9/Lebula%20Pi%207in%20Touchscreen%20Mount/model.png)
![Screen mount](https://bitbucket.org/malhan-designs/x301-mods/raw/e90183a9b56b5ecbf64f9ef5e59a6a5df9950ec9/Lebula%20Pi%207in%20Touchscreen%20Mount/install.png)
