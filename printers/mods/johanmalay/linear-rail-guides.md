---
title: Linear Rail Guides - Johanmalay Mods
description: 
published: true
date: 2022-10-04T17:04:53.800Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/Linear%20Rail%20Guides/
{.is-info}


Use these guides to ensure that your linear rails are aligned properly on the centerline of your extrusion while mounting them. Print 2 of each size to put on either end of the rail and hold it in place while you tighten them down.

# Example Images
![2020 Guide](https://bitbucket.org/malhan-designs/x301-mods/raw/46518d53b57ea2770ad3cdeb065ba70d4080c494/Linear%20Rail%20Guides/model-2020.png)
![2040 Guide](https://bitbucket.org/malhan-designs/x301-mods/raw/46518d53b57ea2770ad3cdeb065ba70d4080c494/Linear%20Rail%20Guides/model-2040.png)

