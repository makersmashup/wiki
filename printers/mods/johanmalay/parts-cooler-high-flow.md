---
title: High Flow Parts Cooler - Johanmalay Mods
description: 
published: true
date: 2022-10-04T16:25:56.034Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/Parts%20Cooler%20-%20High%20Flow/
{.is-info}

This model dramatically increases the flow from the parts cooling fan over the standard model. Included is a new mounting bracket for the 5015 fan/duct and a new fan duct.

I've included the F3D source file for the Cooling Fan Mount, in case you want to design your own duct based on this mount.

Derived from: 

* https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/Cooling%20Fan%20Mount%20Plate.f3d
* https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/Part%20Cooling%20Duct.f3d

# Additional parts (not required)

* The new fan mount provides clearance to install normal M3 x 12mm socket head cap screws in both of the lower holes where the mounting bracket attaches to the carriage. So you can get and install 2x socket head cap screws there if so desired.

# Changes from Original Design

* Rotates the 5015 fan axially by 8* to move the mounting point for the fan duct inboard of the slim belt mount model.
* Moves the fan away from the carriage by 1.5mm to increase flow at fan entry point.
* Removes section of mount inbetween the two top screws that attach to the carriage to increase flow at fan entry point.
* Increased the size of the duct exit from 3mm x 14mm to 4.5mm x 22mm (1.5mm height x 8mm width increase)
* Changed the shape of the duct as air flows through it to minimize bottlenecks.
* Changed the shape of the duct exit to tune air direction.
* Added wall down inner centerline of the duct to aid in printing and direct flow.

# Duct Options

* **Cooling Fan Duct - High Flow** - This works with the X301 Kit extruder / hotend combo, a BMG / V6 clone
* **Cooling Fan Duct - High Flow - Mosquito Long** - This works with a genuine BMG-M / Slice Engineering Mosquito with the heater element facing outward (to give clearance for the duct). This is the recommended configuration for the Mosquito.
* **Cooling Fan Duct - High Flow - Mosquito Short** - This works with a genuine BMG-M / Slice Engineering Mosquito with the heater element facing inward.
* **Cooling Fan Duct - High Flow - Nova** - This is similar to the standard duct, but 1.5mm shorter to work with the Nova hotend

# Printing Notes

* If you're having trouble with the inner centerline wall showing up correctly in your slicer, double check your "horizontal expansion" and "hole horizontal expansion". If either of those is too far negative, it can make that line disappear.
* Print 1x mount at .2mm layer height.
* Print 1x of your chosen duct type at .1mm layer height, with the long portion of the body on the build plate, like the stock fan duct.

# Example Images

## Mounting Bracket
![Parts Cooler - Mounting Bracket](https://bitbucket.org/malhan-designs/x301-mods/raw/fb685197807ce9af6a25e2c5833d06cc2d74d7a5/Parts%20Cooler%20-%20High%20Flow/model-mount.png)

## Fan Ducts

### BMG / V6 Clone
![Parts Cooler - Fan Duct](https://bitbucket.org/malhan-designs/x301-mods/raw/fb685197807ce9af6a25e2c5833d06cc2d74d7a5/Parts%20Cooler%20-%20High%20Flow/model-duct.png)

### Slice Engineering Mosquito - Short (heater inward)
![Parts Cooler - Fan Duct](https://bitbucket.org/malhan-designs/x301-mods/raw/55e25e09d7432d9ec80a6b4c0d7fb902fe8f77be/Parts%20Cooler%20-%20High%20Flow/model-duct-mosquito-short.png)

### Slice Engineering Mosquito - Long (heater outward)
![Parts Cooler - Fan Duct](https://bitbucket.org/malhan-designs/x301-mods/raw/55e25e09d7432d9ec80a6b4c0d7fb902fe8f77be/Parts%20Cooler%20-%20High%20Flow/model-duct-mosquito-long.png)

## Carriage Assembly with New Parts Cooler
![Parts Cooler - Assembly A](https://bitbucket.org/malhan-designs/x301-mods/raw/fb685197807ce9af6a25e2c5833d06cc2d74d7a5/Parts%20Cooler%20-%20High%20Flow/model-assembly-a.png)
![Parts Cooler - Assembly B](https://bitbucket.org/malhan-designs/x301-mods/raw/fb685197807ce9af6a25e2c5833d06cc2d74d7a5/Parts%20Cooler%20-%20High%20Flow/model-assembly-b.png)
![Parts Cooler - Assembly C](https://bitbucket.org/malhan-designs/x301-mods/raw/fb685197807ce9af6a25e2c5833d06cc2d74d7a5/Parts%20Cooler%20-%20High%20Flow/model-assembly-c.png)

## Carriage Assembly with Short Mosquito Duct

![Parts Cooler - Assembly Mosquito](https://bitbucket.org/malhan-designs/x301-mods/raw/fc07c02ecb75595a5feed165f71576e9efb42dc1/Parts%20Cooler%20-%20High%20Flow/model-assembly-m.png)