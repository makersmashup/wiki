---
title: ADXL345 PCB Cable Mount - Johanmalay Mods
description: 
published: true
date: 2022-10-04T16:28:26.661Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/Cable%20PCB%20Mount%20-%20ADXL345/
{.is-info}

This is a simple mod, adding screw holes to the stock cable pcb mount to allow an Adafruit ADXL345 accelerometer to be mounted to the side of the mount. The accelerometer is used by Klipper firmware to tune the input shaper and remove ringing from prints. There are holes on both sides of the mount so you can choose which side you want it on.

Derived from:
https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/Cable%20PCB%20Mount.f3d

# Hardware Required
* Adafruit ADXL345 accelerometer
* 2x M2 x 8mm cap head screws
* 2x M2 nuts

# Installation Notes
I installed the ADXL345 on the right side of the mount, with the pins soldered to the back of the ADXL345. This then allowed me to run the wires up the bowden tube and over to my Pi using my Bowden / Wiring Side Mount. The spacing of the holes will allow you to install the ADXL in the opposite orientation and then run the wires to the back of the machine as well. It is your choice.

After initial tightening of the screws, be sure to check the screws again after 8-10hrs. The wall of the print will collapse a bit and they will loosen up. You do _not_ want these screws to be loose at all. It is very important for the ADXL to be secure during tuning, and the tuning process by its very nature will work the screws loose.

# Example Image
![Cable PCB Mount - ADXL345](https://bitbucket.org/malhan-designs/x301-mods/raw/e0e5e7684134d5e6013b67a641dc4ec668efeecd/Cable%20PCB%20Mount%20-%20ADXL345/model.png)
