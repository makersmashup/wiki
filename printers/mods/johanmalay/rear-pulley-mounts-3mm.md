---
title: 3mm Rear Pulley Mounts - Johanmalay Mods
description: 
published: true
date: 2022-10-04T16:08:44.156Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/Rear%20Pulley%20Brackets%20-%203mm/
{.is-info}

These rear pulley mounts compliment the Composite XY Brackets by changing the pulley holes to use M3 screws. 

If you have the standard 5mm ID pulleys, print 2x of the pulley spacer to convert from 5mm to 3mm.

Derived from: 
* https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/Rear%20Pulley%20Brackets.f3z

# Additional parts
* 2x M3 x 35mm socket head cap screws - to hold the pulleys in place on the Rear Pulley Brackets (was M5 x 35mm)
* 2x M3 nuts

# Example Images
![Rear Pulley Mounts - 3mm](https://bitbucket.org/malhan-designs/x301-mods/raw/e0e5e7684134d5e6013b67a641dc4ec668efeecd/Rear%20Pulley%20Brackets%20-%203mm/model.png)
![Rear Pulley Spacer](https://bitbucket.org/malhan-designs/x301-mods/raw/e0e5e7684134d5e6013b67a641dc4ec668efeecd/Rear%20Pulley%20Brackets%20-%203mm/model-spacer.png)

