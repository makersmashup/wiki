---
title: Simplified Rear Top Corner Bracket
description: 
published: true
date: 2022-10-04T16:09:54.026Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/Simplified%20Rear%20Corner%20Bracket%20Top/
{.is-info}


This is a much simpler version of the top rear corner brackets that support the rear pulley brackets. They are much thicker and thus more resitant to cracking, they also support button head screws to help distribute clamping forces. These bolt to the outside face of the upright 2020 extrusions. They need to be augmented by 90* corner brackets on the inner corners of the 3x 2020s that converge at each corner, as they do not provide any structural rigidity on their own.

Installing these requires a trick since the t-nuts are semi captive. I found that the best way to install these is to put the 4x vertical button head screws / t-nuts on the brackets, then slide the brackets into the channels from the top without the horizontal 2020 in place that the linear rail attaches to. Slide these brackets down the upright 2020 and out of the way, then install / square up the upper horizontal 2020s. Once the frame is all back to square, you can pull these back up into place, install the horizontal button head screw / t-nut and secure into position.

# Additional parts

* 6x 2020 corner brackets - https://www.amazon.com/gp/product/B07JGNLL27
* 12x M4 button head screws / t-nuts for the frame bracing 90* corner brackets
* 12x M5 x 12mm screws (button head recommended) / t-nuts for the rear corner brackets

# Printing Notes

These, like the original design, have a tendency to warp at the ends of the long arms. As long as the warping is only very slight, it shouldn't impact their functionality. Being mindful of keeping the parts fan off until you need to cool the overhangs for the screw holes should help reduce warping.

# Example Images
![Simplified Rear Corner Bracket Top](https://bitbucket.org/malhan-designs/x301-mods/raw/b65edc1b4726fd474d6c79c2ba5d8da32ba236a6/Simplified%20Rear%20Corner%20Bracket%20Top/model.png)

![Example of Installation with 90* Corner Brackets](https://bitbucket.org/malhan-designs/x301-mods/raw/b65edc1b4726fd474d6c79c2ba5d8da32ba236a6/Simplified%20Rear%20Corner%20Bracket%20Top/example.png)

