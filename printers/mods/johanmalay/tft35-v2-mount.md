---
title: BTT TFT35 V2.0 Mount - Johanmalay Mods
description: 
published: true
date: 2022-10-04T16:44:54.932Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/TFT35%20V2%20Mount/
{.is-info}

This model allows the use of a BTT TFT35 V2.0 touch screen LCD on the X301. The TFT35 V2 does not include a jog wheel, so it only operates in touch mode.

# Hardware Required

* BTT TFT35 V2.0
* 4x M3 x 4mm button head screws to attach the screen to the bezel
* 3x M5 x 16mm socket head cap screws / M5 t-nuts to attach the mount to the extrusion

# Printing Notes

* No supports
* Print face down

# Example Image
![TFT35 V2.0 Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/ac25725fa0faed840e47f382c68e9d2681699aa0/TFT35%20V2%20Mount/model.png)

