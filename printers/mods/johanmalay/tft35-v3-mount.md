---
title: BTT TFT35 V3.0 Mount - Johanmalay Mods
description: 
published: true
date: 2022-10-04T16:46:53.339Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/TFT35%20V3%20Mount/
{.is-info}


This model allows the use of a BTT TFT35 V3.0 touch screen LCD on the X301. 

# Hardware Required

* BTT TFT35 V3.0
* 3x M5 x 16mm socket head cap screws / M5 t-nuts to attach the mount to the extrusion

# Printing Notes

* No supports
* Print face down

# Example Image
![TFT35 V3.0 Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/79825530c443feb0694568b5f4cafba3d69130ef/TFT35%20V3%20Mount/model.png)

