---
title: BTT TFT50 V3 Mount - Johanmalay Mods
description: 
published: true
date: 2022-10-04T16:47:58.478Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/TFT50%20V3%20Mount/
{.is-info}

This model allows the use of a BTT TFT50 V3.0 touch screen LCD on the X301. 

# Hardware Required

* BTT TFT50 V3.0
* 3x M5 x 16mm socket head cap screws / M5 t-nuts to attach the mount to the extrusion

# Printing Notes

* No supports
* Print face down

# Example Image
![TFT50 V3.0 Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/b65edc1b4726fd474d6c79c2ba5d8da32ba236a6/TFT50%20V3%20Mount/model.png)

