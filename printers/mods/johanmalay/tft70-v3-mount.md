---
title: BTT TFT70 V3.0 Mount - Johanmalay Mods
description: 
published: true
date: 2022-10-04T16:49:42.932Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/TFT70%20V3%20Mount/
{.is-info}

This model allows the use of a BTT TFT70 V3.0 touch screen LCD on the X301. 

# Hardware Required

* BTT TFT70 V3.0
* 3x M5 x 16mm socket head cap screws / M5 t-nuts to attach the mount to the extrusion
8 4x M3 x (8mm or 6mm) (cap or button) head screws to mount the screen

# Printing Notes

* No supports
* Print face down

# Example Image
![TFT70 V3.0 Mount](https://bitbucket.org/malhan-designs/x301-mods/raw/c0575368947babf3d02cdac91ea28a738bc5c7bf/TFT70%20V3%20Mount/model.png)

