---
title: XY Motion Gates 2GT - Johanmalay Mods
description: 
published: true
date: 2022-10-04T16:02:59.226Z
tags: 
editor: markdown
---

# Download
> https://bitbucket.org/malhan-designs/x301-mods/src/master/XY%20Motion%20System%20-%20Gates%202GT/
{.is-info}

These files build on the work done in the composite XY Brackets. This adjusts those models and the rear pulley brackets to support genuine Gates 2GT pulleys in place of the normal Chinese GT2 pulleys. The Gates pulleys are taller than the chinese ones.

Derived from: 

* https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/XY%20Bracket%20Bottoms%20V2.f3d
* https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/XY%20Bracket%20Top%20Smooth.f3z
* https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/XY%20Bracket%20Top%20Teeth.f3z
* https://bitbucket.org/makersmashup/x301-models/src/master/Fusion/Rear%20Pulley%20Brackets.f3z

# Additional parts

* 6x Gates 2GT Idler - Toothed / 6mm
* 2x Gates 2GT Idler - Smooth / 6mm
* 2x Gates 2GT Pulley - 20 Tooth / 6mm (optional, for the stepper motors if you wanted all genuine Gates parts)
* 5 meters of Gates Powergrip 2GT 6mm belt
* 4x M3 x 40mm socket head cap screws - to hold pulleys in place on the XY Brackets (was M5 x 40mm)
* 2x M3 x 35mm socket head cap screws - to hold the pulleys in place on the Rear Pulley Brackets (was M5 x 35mm)
* 6x M3 nuts
* 8x M3 x 20mm socket head cap screws - for bracket to linear rail block connection (was M3 x 8mm)

# Changes from Original Design

* For XY Brackets, reference: https://bitbucket.org/malhan-designs/x301-mods/src/master/XY%20Brackets%20-%20Composite/
* Adjusts height of XY Brackets and Rear Pulley Brackets to accomodate taller Gates 2GT pulleys.
* Uses printed 5mm to 3mm stack spacer in pulleys for the rear pulley brackets to tie them together and properly load the bearings.

# Printing Notes

* Print 1x each of the Left / Right XY Bracket STLs
* Print 1x of the pair of Rear Pulley Brackets
* The brackets should be oriented so they are laying on their back when printing. 
* XY Bracket Composite files should be printed with supports, with a placement of "touching buildplate". This is just to support the long arm that connects to the linear rail. I like to print these at .16 line height to get a bit more resolution on the fine details in these models.
* The Rear Pulley Brackets should print without supports.
* Print 4x of the XY Pulley Spacers
* Print 2x of the Rear Pulley Spacers
* 2 versions of each of the pulley spacers have been supplied. One is exactly 3mm to 5mm, another is .1mm oversized on the outer diameter. I had to print the .1 oversized version on my printer to get a tight fit on both the M3 screw and the inside of the pulleys. It is important that it is a tight fit on both sides so the pulley's bearings are spinning instead of the printed part. Try playing with your horizontal expansion to get this dialed in, the spacers are a quick print so just print 1 at a time until you get it right.

# Example Images
## XY Brackets

![XY Brackets - Composite - Left](https://bitbucket.org/malhan-designs/x301-mods/raw/366ab9859990f002a7d6a93a786238b5fc5956d0/XY%20Motion%20System%20-%20Gates%202GT/model-xy-left.png)
![XY Brackets - Composite - Right](https://bitbucket.org/malhan-designs/x301-mods/raw/366ab9859990f002a7d6a93a786238b5fc5956d0/XY%20Motion%20System%20-%20Gates%202GT/model-xy-right.png)
![XY Brackets - Composite - Spacer](https://bitbucket.org/malhan-designs/x301-mods/raw/3035607f34eddef9b3c564d2504bda8224c8492d/XY%20Motion%20System%20-%20Gates%202GT/model-xy-spacer.png)

## Example of orientation in slicer

![XY Brackets - Composite - Slicer](https://bitbucket.org/malhan-designs/x301-mods/raw/f2e63691e49dd4e0a7eab29e53a597e450cdc7a3/XY%20Brackets%20-%20Composite/model-slicer.png)

## Rear Pulley Brackets

![Rear Pulley Brackets](https://bitbucket.org/malhan-designs/x301-mods/raw/3035607f34eddef9b3c564d2504bda8224c8492d/XY%20Motion%20System%20-%20Gates%202GT/model-rear-tower.png)

## Rear Pulley Spacer
![Rear Pulley Spacer](https://bitbucket.org/malhan-designs/x301-mods/raw/366ab9859990f002a7d6a93a786238b5fc5956d0/XY%20Motion%20System%20-%20Gates%202GT/model-rear-tower-spacer.png)