---
title: AC Heatbed - Senshado Mods
description: 
published: true
date: 2022-10-04T15:47:54.978Z
tags: 
editor: markdown
---

Copied with permission from: https://imgur.com/a/52fxK9k

With a mains-powered bed, the PSU is only responsible for powering the remaining components, and as the bed is usually by far the single largest power draw, it can be downsized. Going down to 150 watts means the PSU can be fanless (silent) and should still have enough power to run all of the printer's remaining components.

# Electronics Enclosure

![Components in the electronics enclosure](https://i.imgur.com/HKC4Eus.jpeg)
**Components in the electronics enclosure: 40A AC-DC SSR, Mean Well 150 watt PSU.**

The SSR heatsink was a bit larger than expected, but given how warm some of them can get while in use via PWM, better safe than sorry. The underside is attached to the heatsink with thermal tape. A 40A relay isn't strictly necessary as the amperage shouldn't exceed 6.25A in 120V countries or 3.5A in 220V countries, but staying farther below the rated amperage generally tends to keep components cooler, and 40A relays don't really cost more than 25A relays.

Huge credit to Michael at Teaching Tech for his video on the process, available here: https://www.youtube.com/watch?v=1VyFejiKkSQ

SSR: https://www.sparkfun.com/products/13015
PSU: https://www.amazon.com/LRS-150-24-Switching-Supply-Single-Output/dp/B018RE4CWW/

# Tooling Plate

![MIC 6/ATP 5 tool plate](https://i.imgur.com/LgSu81m.jpeg)
**MIC 6/ATP 5 tool plate, 13" square and 1/4" thick.**

13" is almost exactly 330 mm so this plate fits with the existing MK2A bed hardware, but will need longer mounting screws. I would recommend 50 mm rather than the stock 40 mm.

This plate needed a little bit of filing around the edges for deburring but otherwise came in good shape. I actually ordered the stock MK2A bed to use as a measuring guide for the holes but they're 321 mm from centerline to centerline. I only drilled four holes for mounts plus a hole to mount the strain relief, but will be switching to a three-screw mount based on community advice so will be adding a hole along one of the edges.

To drill the holes I used a standard cordless hand drill, over the wooden project board to catch the bit once it had cut through. I had some 3-in-1 oil on hand but didn't end up using cutting fluid, I just went relatively slowly and paused periodically to vacuum up the shavings.

Ordered from Midwest Steel & Aluminum here: https://www.midweststeelsupply.com/store/castaluminumplateatp5

# Heater Pad

![Keenovo 310x310 mm silicone heater pad](https://i.imgur.com/eIW2Xkm.jpeg)
**Keenovo 310x310 mm silicone heater pad, attached to the plate.**

Keenovo can make custom-shaped heater pads so it should be possible to get full plate coverage if you wish. Their 330x330 pad for the TronXY X5S may already be suitable as it has cutouts in the corners, but I have not tested it. This is a 750 watt heater, nearly double the wattage of the standard MK2A heater. Other companies may make even more powerful heaters, but these may require a thicker plate to avoid warping given how quickly they can heat the bed.

Make sure to clean off the plate first before applying the pad to ensure maximum adhesion.

Available here: https://keenovo.store/collections/custom-keenovo-silicone-heaters/products/keenovo-silicone-heater-pad-for-creality-cr10-3d-printer-build-plate-heatbed-heating-upgrade?variant=8320775225399

# Thermal Fuse

![Thermal Fuse](https://i.imgur.com/DBOzNKW.jpeg)
I wired in a 140 °C thermal fuse. Because of the heat, I used crimp-on butt connectors rather than soldering or the heat-shrink solder connecters I normally use, then wrapped it with kapton tape to avoid shorts. The heater isn't polarized so either wire will do, but the fused wire should be connected to the relay so the heater isn't live if the fuse trips.

It isn't shown in the photos, but once this fuse was wrapped, I used aluminum flue tape to hold it in place. Just check to make sure the tape is properly rated for the heat; not all flue tape is rated to these temperatures, and not all tape sticks well enough. I bought some Nashua tape at the local home improvement store.

The bed wiring may need some extension (for the fused end), in which case the 16 gauge wiring in the BOM is of sufficient gauge for this purpose. Do not use thinner gauge wiring as they will be carrying a decent current (over 6 amps).


# Insulation

![Cork tile insulation](https://i.imgur.com/V8AzYCx.jpeg)
I used cork tile sheets to insulate the bed, held on with more of the flue tape. 13" cork tile is unsurprisingly hard to find so it's just a 12" tile with additional strips taped on.

![Fully wrapped](https://i.imgur.com/V8AzYCx.jpeg)
Fully wrapped, with cutouts in the corners for the bed mounts.

At least in my case, the wiring location meant that the included wire strain relief had to be mirrored to match the hole spacing. I also widened it a little to accept the thicker wiring, including the neopixel wires.

# Installed

![Bed installed](https://i.imgur.com/wBtgpoU.jpeg)
Mounted and in place on the printer.

![Wiring](https://i.imgur.com/EtrW0xd.jpeg)
Some of the electronics wiring. The heatbed itself isn't wired in, but the SSR has already been connected to the PSU.

The general wiring arrangement is:
* The SSR's AC input will be connected to the live terminal of the PSU. The AC output will be connected to the fused wire in the bed heater.
* The SSR's DC positive and negative wires will be connected to the bed heater terminals in the mainboard.
* A grounding harness should be wired together to ground the SSR, bed, and frame to the ground terminal on the PSU.

Passing the bed power through the PSU terminals means that the bed is also controlled by the main power switch and fuse, providing an additional layer of safety. You can also rig up a separate power switch for the bed wiring if you wish.

![Clearance](https://i.imgur.com/uf4jZKe.jpeg)
The cork insulation and slight bulge from the thermistor in the center of the heater brings the bottom of the insulation pretty close to the crossbar, but it should just clear it without touching.