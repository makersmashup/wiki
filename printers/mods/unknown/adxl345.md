---
title: ADXL345 - Mods
description: 
published: true
date: 2022-10-07T00:45:58.570Z
tags: 
editor: markdown
---

This page has been moved [here](/general/firmware/klipper/tuning/adxl345).