---
title: Right Angle Corner Brackets - Mods
description: 
published: true
date: 2022-10-10T16:49:15.393Z
tags: 
editor: markdown
---

# Overview

When building a CoreXY printer, there are a couple core concepts which really determine the overall build quality and ability to perform; Rigidity and Squareness. While not part of the original X-Series spec, some members have added right angle corner brackets to ensure that their printer is as square and rigid as possible.

# Parts Required

- [(20) Right Angle Corner Brackets](https://www.amazon.com/dp/B07GGLYX9V)

# Installation

This section is coming soon.