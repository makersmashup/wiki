---
title: Build Guide
description: 
published: true
date: 2022-10-03T19:39:43.480Z
tags: 
editor: markdown
---

> This guide is a work in progress.  It does not currently describe the full build process.
{.is-warning}

This is the official build guide for the LayerFused X301 printer.  In addition to this textual guide, we have a series of [YouTube videos](https://www.youtube.com/watch?v=1UX8DlmzmM0) that walks through building the printer.  These videos may not hold up over time so this documentation will be the primary documenation

# Prepare

If you have not done so already take a quick look at the BOM listed below. This will have a combination of required parts as well as parts that are optional.
[X301 Bill Of Materials](https://docs.google.com/spreadsheets/d/1BkFmqMdLik8-B1qxVCviEUmhcazlHoreDtXW5ndriQE/edit?usp=sharing)

## Tools / Materials
A number of tools are required during the build.  To make it easier to ensure you have all of the tools you need, we've made a comprehensive list below.  Each step will also contain a list of tools you'll need for that specific step.

**Note**: Linked products are for reference and convenience purposes. If any links are not working, check the [X301 Bill Of Materials](https://docs.google.com/spreadsheets/d/1BkFmqMdLik8-B1qxVCviEUmhcazlHoreDtXW5ndriQE/edit?usp=sharing) for a working link.

### Required

- [Metric wreches/spanners, specifically 7 mm, 8mm and 12mm](https://www.amazon.com/DURATECH-Super-Thin-Wrench-8-Piece-Rolling/dp/B08CVC5BDL?tag=rwmech-20)
- [Wire strippers](https://www.amazon.com/Stripper-Dromild-Automatic-Stripping-Self-adjusting/dp/B081821YSV?tag=rwmech-20)
- [Insulated terminal crimper](https://amzn.to/2KN7ggw)
- [40W+ Soldering iron](https://www.amazon.com/gp/product/B07FCX1M6Q?tag=rwmech-20) and [set of tips](https://www.amazon.com/Soldering-Tabiger-X-Tronic-Station-Different/dp/B077V1VND5?tag=rwmech-20)
- [Heat shrink](https://www.amazon.com/270-pcs-Adhesive-Assortment-MILAPEAK/dp/B0771K1Z7Q?tag=rwmech-20) or [vinyl electrical tape](https://www.amazon.com/Scotch-Super-Vinyl-Electrical-Tape/dp/B00004WCCL?tag=rwmech-20)
- [JST-XH pin crimper](https://www.amazon.com/IWISS-IWS-3220M-Connector-0-03-0-52mm%C2%B2-Ratcheting/dp/B078WPT5M1?tag=rwmech-20) ![lflogo.svg](/lflogo.svg =10x)
- Power drill with 5mm bit ![lflogo.svg](/lflogo.svg =10x)

#### Screwdrivers

If you are looking for exactly the screwdrivers and bit to get you started, the first few links should do nicely.

- [Metric hex bits or keys, specifically 1.5, 2, 2.5 and 4mm](https://amzn.to/3f3GfDz)
- [Precision screwdrivers/bits](https://www.amazon.com/Screwdriver-Precision-Different-Flathead-Screwdrivers/dp/B07QYL9KJT?tag=rwmech-20)

If you are looking for a nice high quality screwdriver set with many bits, the iFixit Toolkits are hard to beat.

- [iFixIt Moray Toolkit](https://www.amazon.com/iFixit-Moray-Driver-Kit-Smartphones/dp/B08NWKMT8V?tag=rwmech-20)
- [iFixIt Manta Toolkit](https://www.amazon.com/iFixit-Manta-Driver-Kit-Piece/dp/B07BMM74FD?tag=rwmech-20)

### Recommended
- 3D Printer with at least 120mm^3^ build volume
- [Craft knife](https://www.amazon.com/REXBETI-Precision-Suitable-Scrapbooking-Sculpture/dp/B07VN5BN48?tag=rwmech-20)
- [Multimeter](https://amzn.to/3dwTGKX) (or [alternative](https://www.amazon.com/Southwire-Tools-Equipment-21510N-Clamp/dp/B07H9WVJDS?tag=rwmech-20))
- [Heat gun](https://amzn.to/3dw4pVU)
- [Ferrules with crimper](https://amzn.to/35mUz5u)
- Drill press ![lflogo.svg](/lflogo.svg =10x)


![lflogo.svg](/lflogo.svg =10x) *Not required when buying [LayerFused X301 Complete Kit](https://shop.layerfused.com/products/layerfused-x301-complete-kit)*


## Printed Parts
The build leverages several plastic parts which can be printed or purchased.

### Purchase
We offer an [X301 printed parts kit](https://shop.layerfused.com/collections/printer-kits/products/x301-printed-parts-kit) on the LayerFused store.  This kit includes all parts required to get the printer up and running.  Some additional parts will need to be printed on your X301 prior to putting the finishing touches on your printer.

If you live outside the USA, it may be cheaper to find a local service or individual to print these parts for you.

### Print yourself
All parts for the printer are available in our [Bitbucket repository](https://bitbucket.org/makersmashup/x301-models/src/master/).  STL files are available in the STL folder.  See the [X301 Printing Guide](/X301/printing-guide) for specifics on printing each part.

If you do not have a printer, you can ask a friend or look for local maker spaces that have printers you can use.  We'd recommend buying at least 2kg of filament to ensure you have enough to print the parts including potential failures.

# Building the frame
The Frame is the most important part of the 3D printer.  This needs to be rigid and square.  Take your time and ensure that everything is put together properly and as square as possible.

**Hardware required for frame:**
3 x 400mm 2080 Aluminum extrusion
2 x 400mm 2040 Aluminum extrusion
7 x 400mm 2020 Aluminum extrusion
12 x Aluminum T slot L shape joining plate 
10 x Inside corner bracket
72 x 8mm M5 Button head screws
12 x 8mm M5 Socket head screws
84 x M5 T-Nuts

**Printed Parts for Frame:**
2 x Rear Corner Bracket Tops
4 X TPU Feet

[Frame Assembly Step-By-Step](/en/X301/Build-Guide/Frame)

# Assemble the motion system
**Hardware required for motion system:**
4 x Flexible Shaft Coupling
2 x Stainless Steel Lead Screw 400mm T8 OD 8mm pitch 2mm lead w/ nut
5 x MGN12H Linear Rail 400mm long, Single Block
4 x Nema 17 1.8 deg high torque stepper motors
1 x Pancake Stepper Motor
1 x GT2-6mm Belt (5 Meters minimum)
6 x GT2 Idler Pulley 20 Teeth 5mm bore 6mm Width
2 x GT2 Idler Pulley Toothless 5mm bore 6mm Width
2 x GT2 Pulley 20 Teeth 5mm Bore 6mm Width (X/Y Stepper Motors)
2 x M5 35mm Socket Head Screws
2 x M5 Washers
4 x M5 40mm Socket Head Screws
6 x M5 12mm Socket Head Screws
22 x M5 Nuts
12 x M5 8mm Socket Head Screws
42 x M3 8mm Socket Head Screws
1 x M3 8mm Button Head Screw
8 x M3 12mm Socket Head Screws
8 x M3 Washer
2 x M4 20mm Socket Head Screws

**Printed Parts for Motion System:**
Left & Right Rear Pulley Towers
Left & Right X/Y Motor Mounts
2 x Z Carriage Brackets
2 x Z Motor Mounts
1 x Filament Feed Mount
1 x Cable Plate Mount (Solder Version/PCB Version)
1 x Carriage - Front Plate
1 x Carriage - Belt Main Body
1 x Carriage - Belt Plate Right
1 x Carriage - Belt Plate Left
1 x Cooling Fan Plate
1 x Part Cooling Duct (Volcano)

[Motion System Step-By-Step](/en/X301/Build-Guide/motion-system)

# Assemble the print bed
**Hardware required for print bed:**
3 x 300mm 2020 Aluminum extrusion
1 x 3D Printer Heat Bed Mk2a or CR-10 24v
4 x Silicone Leveling Column
1 x Dual Sided PEI Build Plate (Optional but recomended)
20 x M5 8mm Socket Head Screws
20 x M5 T-Nuts
**Printed Parts for Bed Assembly:**
2 x Bed Brackets
4 x Bed Mounts (MK2A or CR-10)

[Bed Assembly Step-By-Step](/en/X301/Build-Guide/bed-assembly)

# Mount & Wire the electronics
Manage those cables! 
**Printed Parts for Electronics:**
1 x Rear power panel mount
1 x Mosfet Mount
1 x Mainboard Mount
1 x Rear Cable Mount
1 x Mechanical Endstop mount

[Electronics Step-By-Step](/en/X301/Build-Guide/electronics)

# Load the firmware
How do I make it work?
LayerFused maintains a fork of the Marlin Firmware.  This fork is tested throughly for bugs and issues before it is updated to the latest version to ensure that everything works well with the printers.

**Before you Begin**
-Make sure you have VS Code with the Platform IO plugin installed on your machine.
-Clone or Download the Repositiory from the [LayerFused BitBucket](https://bitbucket.org/makersmashup/marlin/src/2.0.x-x301/)

[Firmware Step-By-Step](/en/X301/Build-Guide/firmware)

# Testing
Does it work right?

# Calibration
Check all the things

# Troubleshooting
## Gantry is not square
This is usually caused by one of two things. Either the frame is not square or you have one belt longer than the other.  Adjust your belt by one or two teeth and this usually alligns the gantry so it is square.  Leave extra belt on both sides until you're satisfied before trimming.




# What to do next
Print something AWESOME! 