---
title: Build - X301
description: 
published: true
date: 2022-10-22T14:14:51.240Z
tags: x301, build
editor: markdown
---

This section contains all the information you need to get started building your own LayerFused X301.

- [Prepare](prepare)
- [Build the Frame](frame)
- [Build the Motion System](motion)
- [Build the Print Bed](bed)
- [Mount the Electronics](electronics-mounting)
- [Build the Carriage Assembly](carriage)
- [Connect the Electronics](electronics)