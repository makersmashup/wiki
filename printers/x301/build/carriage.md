---
title: Carriage Assembly - Build X301
description: 
published: true
date: 2022-10-22T14:31:07.775Z
tags: 
editor: markdown
---

# Overview

The carriage itself is another component that varies heavily depending on several factors such at hotend, fan, type (direct drive or bowden), etc. There are several commonalities within X301 builds in regards to belt pathing, and core carriage design. In this section, we will walk through a standard build with a pancake stepper motor, BMG style extruder, and E3D Volcano hotend.

# Hardware

## Purchased Parts

- (1) [Aluminum Carriage Plate](https://shop.layerfused.com/products/x301-aluminum-carriage-plate) (or 3D Printed option below)
- (1) 20mm Nema 17 Pancake Stepper Motor
- (1) Blower Fan
- (X) Xmm M3 Screws
- (X) M3 Nuts
- (2) 40mm M4 Screws
- (X) Xmm M5 Screws
- (X) M5 Nuts

## Printed Parts

- (1) [Carriage Plate](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Carriage%20Plate%20P3%20Printer.stl) (or recommended [Aluminum Carriage Plate](https://shop.layerfused.com/products/x301-aluminum-carriage-plate))
- (2) [Carriage Belt Clamp](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Carriage%20Belt%20Clamp.stl) (or [Slim](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Carriage%20Belt%20Clamp%20Slim.stl))
- (1) [Carriage Belt Body](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Carriage%20Belt%20Body.stl) (or [Slim](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Carriage%20Belt%20Body%20Slim.stl))
- (1) [Cable Plate](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Cable%20Plate.stl)
- (1) [Cooling Fan Mount Plate](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Cooling%20Fan%20Mount%20Plate.stl)
- (1) [Fan Duct](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Fan%20Duct.stl)

# Mods to Consider

Coming soon...

# Video

[DIY CoreXY 3D Printer Build | LayerFused X301 Part 4 - Carriage Assembly](https://www.youtube.com/watch?v=R5psYLsCUlk)

# Instructions

Coming soon...

[Previous Step](./electronics-mounting) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Back to Guide](.) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Next Step](./electronics)