---
title: Electronics Mounting - Build X301
description: 
published: true
date: 2022-10-22T18:44:22.069Z
tags: 
editor: markdown
---

# Overview

This step will deviate quite a bit from the formatting of the previous steps as this is one part where nearly all X301's are a little different. Depending on which exact parts you chose to purchase for you X301 and how you want to mount them, your X301 will likely turn out very different than the one built in the video.

# Video

[DIY CoreXY 3D Printer | LayerFused X301 Part 3 - Electronics Mounting](https://www.youtube.com/watch?v=-oRuqZ1-NYI)

# Hardware

## Purchased Parts

The parts you want to use will vary wildly based on things such as new control boards that have been released, what is available for purchase when you build your X301, and the power requirements of your components.

To help you navigate this section, we will take a look at the components used in the video and talk about the considerations that went into place to wind up at those components.

### Power Supply (PSU)

If you are looking to build a 3D Printer these days, you are likely looking at 24v as many components in the printer work better at higher volts and a higher voltage also has the benefit of allowing thinner gauge wire to be used with the same wattage draw. 20A will give you 480W at 24v, which should be more than enough for a 60W heat cartridge, 240W heated bed, and the various other components.

When choosing a power supply, it is recommended that you determine the combined wattage of all parts, then choose a PSU that is rated for your wattage plus 10-20%, so if as long as your needs to not exceed around 400W, a 480W power supply will do fine. A very popular brand within the DIY scene is Mean Well, who are known for their high quality PSU's. Their LRS lineup is a good choice, but it only goes up to 350W (LRS-350-24). If you want to go higher, they have a 600W PSU (SE-600-24), but be aware that it is very big. That said, it will still fit in the electronics compartment, so it isn't a bad choice.

Another consideration is noise. Going back to Mean Well, all of their PSU's above 200W (LRS-200-24) have active cooling fans which are audible at best and loud at worst. You COULD modify them with something like a Noctua fan to make them silent, but you could also shuffle some other components around to meet your needs. One such modification would be to use a 110V/220V heated bed as discussed below.

### Mosfets and Solid State Relays (SSRs)

When choosing how to power your more intense components such as the heated bed and heater cartridge, you will see Mosfets and SSRs tossed around. These devices allow you to use the control board to control when the component is recieving power, while not requiring the power to go through the board itself. While control boards should be built to withstand a descent amount of current, it is unlikely that they will be built to withstand as much current as a power supply or device whose sole purpose is to handle large amounts of current. At the very least, these devices will increase the longevity of your control board and may increase the safety of operation.

> It is rare, but not unheard of, for theses devices to fail, and when they do fail, they tend to do so in a way which allows the flow of electricity to continue. This could lead to a catastrophic event such as a fire. To help safeguard against this possability, one should make sure that they purchase their mosfets and SSRs from a reputable source. Additionally, you may wish to consider incorporating a thermal fuse, which 
{.is-warning}

### Control Board

New control boards are coming out constantly. One of the biggest and most well known manufacturers is BIGTREETECH/BIQU. It is worth checking out their latest products to see what they are currently offering. At the time of this writing, the Manta M8P was a solid choice as it offered 8 stepper drivers and a slot for a computer in the form factor of the Raspberry Pi CM4. They also released their CB1, which is an alternative to the CM4, but with the added benefit of actually being available to purchase for a reasonable price. Around OCT2022, you could find an M8P, 8 TMC2209 stepper drivers, and a CB1 with heatsink for around $100 directly from BIQU's store.

Some of the key factors to consider are that it must be able to handle 24v input if you are using a 24v PSU and that it may make for a cleaner build if you have a large number of stepper drivers on one board. The stock X301 has a total of 5 stepper motors (2 XY, 2 Z, and 1 extruder), and a common mod is to add a 6th for 3 Z steppers. The point being, while it is possible to use Klipper to control multiple control boards, it probably makes sense to have a little room to grow out of the gate.

### Heated Bed

The stock build calls for a 240W 24V Mk2A heated build plate, and while that is a fine recommendation, some users have chosen to run a Keenovo 750W 120V heated build plate. The benefits to doing this are that you could choose a smaller, potentially fanless, PSU and the heating of the bed will be faster and more efficient. The downsides to going this route are that if the SSR fails, the bed could potentially continue to get hotter with the control board being unable to stop it. With that in mind, adding a thermal fuse at the heated bed which will blow if the bed gets too hot is a very good idea.

> A 120v heated bed has fire safety implications that can be mitigated with the proper planning. Ensure you understand and accept the risks before going that route.
{.is-warning}

### Displays

There are a very wide number of displays out there from simple screens with rotory dials to full touch screens. In the video, a relatively simple screen was chosen. Do your research to determin which type of screen you would like to use or if you would like to forego the screen all together.

## Printed Parts

This section contains 3D printed parts used in the video

### Extrusion Covers

- [2020 Extrusion Cover](https://bitbucket.org/makersmashup/x301-models/src/master/STL/2020%20Cover.stl)
- [2020 Extrusion Cover Cutout](https://bitbucket.org/makersmashup/x301-models/src/master/STL/2020%20Cover%20Cutout.stl) (or [Mirrored](https://bitbucket.org/makersmashup/x301-models/src/master/STL/2020%20Cover%20Cutout%20Mirrored.stl))

### Rear Plates

- [Rear Vent Grill](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Rear%20Vent%20Grill.stl) (or [2mm Smaller](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Rear%20Vent%20Grill%20(2mm%20Smaller).stl))
- [Rear USB Port](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Rear%20USB%20Port.stl)
- [Rear Power Panel](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Power%20Rear%20Panel.stl)

### Mounts

- [Mosfet Mount](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Mosfet%20Mount.stl) (or [Large](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Mosfet%20Mount%20Large.stl))
- [SKR Control Board Mount](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Mainboard%20Mount.stl)
- [Reverse Bowden Mount](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Bowden%20Mount.stl)

### Other

- [Heat Bed Cable Relief](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Heat%20Bed%20Cable%20Relief.stl)
- [PSU Fan Duct](https://bitbucket.org/makersmashup/x301-models/src/master/STL/Fan%20Duct.stl)

[Previous Step](./bed) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Back to Guide](.) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Next Step](./carriage)