---
title: Connect the Electronics - Build X301
description: 
published: true
date: 2022-10-22T14:13:04.711Z
tags: 
editor: markdown
---

# Overview

Manage those cables! 

# Hardware

You will need the following 3D Printed parts:

- (1) Rear power panel mount
- (1) Mosfet Mount
- (1) Mainboard Mount
- (1) Rear Cable Mount
- (1) Mechanical Endstop mount

# Video

[DIY CoreXY 3D Printer | LayerFused X301 Part 3 - Electronics Mounting](https://youtu.be/-oRuqZ1-NYI)

# Instructions

## Mounting Electronics

There really isnt a step-by-step for mounting the electronics in the electronics box.  The box is large enough to accomodate a number of different configurations.  The main thing to keep in mind when mounting and installing the electronics is that cable management is important.  The Signal cable was used with this in mind.

[Previous Step](./carriage) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Back to Guide](.)
