---
title: Prepare - Build X301
description: 
published: true
date: 2022-10-04T01:03:44.487Z
tags: x301, build, guide
editor: markdown
---

# Prepare

In order to start building the LayerFused X301, you must first take several steps to ensure you have everything you need.

## Bill of Materials (BoM)

If you have not done so already take a quick look at the BOM listed below. This will have a combination of required parts as well as parts that are optional.

[X301 Bill Of Materials](https://docs.google.com/spreadsheets/d/1BkFmqMdLik8-B1qxVCviEUmhcazlHoreDtXW5ndriQE/edit?usp=sharing)

## 3D Printed Parts

The build leverages several plastic parts which can be printed or purchased.

### Purchase

We offer an [X201 printed parts kit](https://shop.layerfused.com/collections/printer-kits/products/copy-of-layerfused-x301-printed-parts-kit) on the LayerFused store.  This kit includes all parts required to get the printer up and running.  Some additional parts will need to be printed on your X301 prior to putting the finishing touches on your printer.

If you live outside the USA, it may be cheaper to find a local service or individual to print these parts for you.

### Print yourself

All parts for the printer are available in our [Bitbucket repository](https://bitbucket.org/makersmashup/x301-models/src/master/).  STL files are available in the STL folder.  See the [Printing Guide](printing-guide) for specifics on printing each part.

If you do not have a printer, you can ask a friend or look for local maker spaces that have printers you can use.  We'd recommend buying at least 2kg of filament to ensure you have enough to print the parts including potential failures.

## Tools

There are a number of common tools that every maker should find in their arsenal. We have compiled a list of tools that will help you successfully build the X301 on the [General - Tools](/general/tools) page.

[Back to Guide](.) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Next Step](./frame)