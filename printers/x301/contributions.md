---
title: Contributions
description: 
published: true
date: 2022-10-03T19:56:39.840Z
tags: 
editor: markdown
---

# 3rd Party Contributions

[Johanmalay's Mods](/printers/x301/contributions/johanmalay-mods)
[Senshado's AC Heatbed DIY](/printers/x301/contributions/senshado-ac-heatbed)
[Dual-Z Marlin Config](/printers/x301/contributions/dual-z-configuration)
[Bwnance's Mods](/printers/x301/contributions/bwnance-mods)
[PeterS Mods](/printers/x301/contributions/peters-mods)