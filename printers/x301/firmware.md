---
title: Firmware
description: 
published: true
date: 2022-10-03T19:53:22.531Z
tags: 
editor: markdown
---

## Firmware

This printer has been tested by our community with the following firmwares, but for the best support, we recommend starting out with Marlin.

- [Marlin](/printers/x301/firmware/marlin)
- [Klipper](/printers/x301/firmware/klipper)
- [RepRap](/printers/x301/firmware/reprap)