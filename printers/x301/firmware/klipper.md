---
title: Klipper
description: 
published: true
date: 2022-10-04T01:12:30.116Z
tags: 
editor: markdown
---

### Klipper

Klipper Firmware has been tested and used on Layerfused printers and shown great success running a variety of different hardware.  Duet boards, SKR Pro, BTT GTR have been used on LayerFused printers.

For more information about Klipper, check out the [General Klipper](/general/firmware/klipper) page.