---
title: Marlin
description: 
published: true
date: 2022-10-04T01:11:36.129Z
tags: 
editor: markdown
---

# Marlin

The LayerFused team maintains a [fork](https://bitbucket.org/makersmashup/marlin) of Marlin for this printer.

For general information about Marlin, check out the [General Marlin Firmware](/general/firmware/marlin) page.