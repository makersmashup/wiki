---
title: Mods
description: 
published: true
date: 2022-10-03T23:17:44.849Z
tags: 
editor: markdown
---

# [Mods](mods)

Some of our community members have worked hard to improve or otherwise modify the original X301 design. Information about their efforts can be found below.

## [Bwnance](mods/bwnance)
- [Pillow Blocks](mods/bwnance/pillow-blocks)
- [3 Z Magnetic Bed Mod](mods/bwnance/magnetic-bed)
## [Johanmalay](/X301/contributions/johanmalay-mods)

Johanmalay has contributed many mods and great writeups for them.	You can find them [here](/X301/contributions/johanmalay-mods) until the wiki restructure is complete.

## [Senshado](mods/senshado)
- [AC Heatbed](mods/senshado/ac-heatbed)
## [Unknown](mods/unknown)
- [Dual-Z Axis Configuration](mods/unknown/dual-z-axis)
